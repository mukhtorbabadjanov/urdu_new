<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'fsp' => true,
    'rna' => false,
    'lwe' => false,
    'bsVersion' => '4.x', // this will set globally `bsVersion` to Bootstrap 4.x for all Krajee Extensions
    'user.spamNames' => 'admin|superadmin|creator|thecreator|username',
    'user.passwordResetTokenExpire' => 3600,
    'supportEmail' => 'robot@urdu.uz',
    'PostsImgDir'=>'/uploads/images/posts/',
    'mylanguages'=>['uz'=>'O\'zbek','ru'=>'Русский','en'=>'English'],
];
