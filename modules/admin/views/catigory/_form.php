<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\models\Search;
$searchform = new Search();
$mymenu=$searchform->myCatParentById();
?>
<div class="catigory-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'parent')->dropDownList($mymenu,['prompt' => ' -- Танланг --']);?>
    <?= $form->field($model, 'tag')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'template')->dropDownList([''=>"Андозасиз", 'article'=>'Якка саҳифали','page'=>"Саҳифалар"],['prompt' => ' -- Танланг --']);?>
    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>
</div>