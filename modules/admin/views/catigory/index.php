<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CatigorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'),'template'=>"<li class='breadcrumb-item'>{link}</li>"];
?>
<div class="catigory-index">

    <h1 class="text-uppercase"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'', // скрыть
        'pager' => [ 'class' => '\yii\bootstrap4\LinkPager'],
      
        'columns' => [

            ['attribute' => 'id', 'label'=>'ID', 'headerOptions' => ['width' => 30]],

           ['attribute' => 'name_uz', 'label'=>'Kategoriya (uzbek)', 'headerOptions' => ['width' => '25%']],
           ['attribute' => 'name_ru', 'label'=>'Kategoriya (рус)', 'headerOptions' => ['width' => '25%']],
           ['attribute' => 'name_en', 'label'=>'Kategoriya (english)', 'headerOptions' => ['width' => '25%']],

            //'template',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
