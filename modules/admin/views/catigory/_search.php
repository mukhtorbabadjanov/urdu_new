<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CatigorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="catigory-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'cat_id') ?>

    <?= $form->field($model, 'catigory_uz') ?>

    <?= $form->field($model, 'catigory_ru') ?>

    <?= $form->field($model, 'catigory_en') ?>

    <?= $form->field($model, 'catigory_es') ?>

    <?php // echo $form->field($model, 'catigory_ko') ?>

    <?php // echo $form->field($model, 'catigory_ja') ?>

    <?php // echo $form->field($model, 'catigory_de') ?>

    <?php // echo $form->field($model, 'catigory_fr') ?>

    <?php // echo $form->field($model, 'catigory_zh') ?>

    <?php // echo $form->field($model, 'menu_id') ?>

    <?php // echo $form->field($model, 'template') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
