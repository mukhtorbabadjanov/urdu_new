<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index'],'template'=>"<li class='breadcrumb-item'>{link}</li>"];
$this->params['breadcrumbs'][] = ['label' => $this->title,'template'=>"<li class='breadcrumb-item'>{link}</li>"];

?>
<div class="catigory-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php
$script = <<< JS
$(function() {
    $('#catigory-template').change(function () {
        
        var tag=$('#catigory-tag').val();
        var template=$('#catigory-template').val();
     //   var url=$('#catigory-url').val();
     url='/site/';
     if (template!=='')  url=url+template+'/';
        document.getElementById('catigory-url').value=url+tag;
    
    
        return true;
        
        
        
    });      
            
    });
JS;
$this->registerJs($script);
?>