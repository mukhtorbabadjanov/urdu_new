<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Photogallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="photogallery-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'image')->fileInput() ?>
 <div id="isop">
     <fieldset class='border p-2  formuz'>
         <legend class='w-auto'> O'zbek</legend>
         <button class='btn btn-outline-warning' id='uz'><i class='fa fa-cut'></i></button>
         <?= $form->field($model, 'title_uz')->textInput(['maxlength' => true]) ?>
         <?= $form->field($model, 'text_uz')->textarea(['rows' => 6]) ?>
     </fieldset>

     <fieldset class='border p-2  formru'>
         <legend class='w-auto'> Русский</legend>
         <button class='btn btn-outline-warning' id='ru'><i class='fa fa-cut'></i></button>
         <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>
         <?= $form->field($model, 'text_ru')->textarea(['rows' => 6]) ?>
     </fieldset>

     <fieldset class='border p-2  formen'>
         <legend class='w-auto'> English</legend>
         <button class='btn btn-outline-warning' id='en'><i class='fa fa-cut'></i></button>
         <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>
         <?= $form->field($model, 'text_en')->textarea(['rows' => 6]) ?>
     </fieldset>
 </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS
    $('#uz').click(function () {
        $('#isop .formuz').remove(); 
});
    $('#ru').click(function () {
        $('#isop .formru').remove(); 
});
    $('#en').click(function () {
        $('#isop .formen').remove(); 
});
JS;
$this->registerJs($script);
?>