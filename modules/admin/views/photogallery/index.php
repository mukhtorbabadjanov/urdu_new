<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhotogallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Photogalleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photogallery-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Photogallery', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'image',
            'title_ru',
            'title_en',
            'title_uz',
            //'text_uz:ntext',
            //'text_en:ntext',
            //'text_ru:ntext',
            //'type',
            //'image_cat',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
