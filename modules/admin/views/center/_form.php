<?php

use app\models\Search;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\InputFile;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Center */
/* @var $form yii\widgets\ActiveForm */
$searchform = new Search();
$myUser = $searchform->myUserParentById();
$mymenu = $searchform->myCatParentById();

?>

<div class="center-form">

    <?php $form = ActiveForm::begin(); ?>
    <div id="isop">

    <fieldset class='border p-2  formuz'>
        <legend class='w-auto'> O'zbek</legend>
        <button class='btn btn-outline-warning' id='uz'><i class='fa fa-cut'></i></button>
    <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'content_uz')->widget(CKEditor::className(),[
            'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions(['elfinder', 'path' => 'files/public'],[])
        ]); ?>
    </fieldset>

    <fieldset class='border p-2  formru'>
        <legend class='w-auto'> Русский</legend>
        <button class='btn btn-outline-warning' id='ru'><i class='fa fa-cut'></i></button>
    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'content_ru')->widget(CKEditor::className(),[
            'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions(['elfinder', 'path' => 'files/public'],[])
        ]); ?>
    </fieldset>

    <fieldset class='border p-2  formen'>
        <legend class='w-auto'> English</legend>
        <button class='btn btn-outline-warning' id='en'><i class='fa fa-cut'></i></button>
    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'content_en')->widget(CKEditor::className(),[
            'editorOptions' => \mihaildev\elfinder\ElFinder::ckeditorOptions(['elfinder', 'path' => 'files/public'],[])
        ]); ?>
    </fieldset>
    </div>
    <?php

    echo $form->field($model, 'image')->widget(InputFile::className(), [
        'language'      => Yii::$app->language,
        'controller'    => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
        'path' => 'public', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
        'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
        'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'options'       => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-info'],
        'multiple'      => false       // возможность выбора нескольких файлов
    ]);

    ?>

    <?= $form->field($model, 'user_id')->dropDownList($myUser, ['prompt' => ' -- Танланг --']); ?>

    <?= $form->field($model, 'fak_id')->textInput() ?>

    <?= $form->field($model, 'cate')->dropDownList($mymenu, ['prompt' => ' -- Танланг --']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS
    $('#uz').click(function () {
        $('#isop .formuz').remove(); 
});
    $('#ru').click(function () {
        $('#isop .formru').remove(); 
});
    $('#en').click(function () {
        $('#isop .formen').remove(); 
});
JS;
$this->registerJs($script);
?>
