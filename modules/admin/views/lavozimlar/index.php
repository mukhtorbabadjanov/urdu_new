<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\LavozimlarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Lavozimlar');
$this->params['breadcrumbs'][] = $this->title;
$mylavozim=[
    '1'=>Yii::t('app','University administration'),
    '2'=>Yii::t('app','Directors and deans'),
    '3'=>Yii::t('app','Heads of department'),
    '4'=>Yii::t('app','Professors'),
    '5'=>Yii::t('app','Associate professors'),
    '6'=>Yii::t('app','Assistants and Teachers'),
    '7'=>Yii::t('app','Heads of office'),
    '8'=>Yii::t('app','Researchers'),
    '9'=>Yii::t('app','Specialist'),
];
?>
<div class="lavozimlar-index">

    <h1 class="text-uppercase"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php

    ?>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'', // скрыть
        'pager' => [ 'class' => '\yii\bootstrap4\LinkPager'],
        'columns' => [


            ['attribute' => 'id', 'label'=>'ID',  'headerOptions' => ['width' => 15]],
            'name_uz',
            'name_ru',
            'name_en',
            ['attribute' => 'cat', 'label'=>Yii::t('app',"Catigorylavozim"),
                'filter' =>$mylavozim,
                'format' => 'raw',
                'value' => function ($model) {
                    $active = $model->cat ;
                    $mylavozim=[
                        '1'=>Yii::t('app','University administration'),
                        '2'=>Yii::t('app','Directors and deans'),
                        '3'=>Yii::t('app','Heads of department'),
                        '4'=>Yii::t('app','Professors'),
                        '5'=>Yii::t('app','Associate professors'),
                        '6'=>Yii::t('app','Assistants and Teachers'),
                        '7'=>Yii::t('app','Heads of office'),
                        '8'=>Yii::t('app','Researchers'),
                        '9'=>Yii::t('app','Specialist'),
                    ];

                    return $mylavozim[$active];
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
