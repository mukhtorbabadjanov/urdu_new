<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm

/* @var $this yii\web\View */
/* @var $model app\models\Lavozimlar */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$mylavozim=[
    '1'=>Yii::t('app','University administration'),
    '2'=>Yii::t('app','Directors and deans'),
    '3'=>Yii::t('app','Heads of department'),
    '4'=>Yii::t('app','Professors'),
    '5'=>Yii::t('app','Associate professors'),
    '6'=>Yii::t('app','Assistants and Teachers'),
    '7'=>Yii::t('app','Heads of office'),
    '8'=>Yii::t('app','Researchers'),
    '9'=>Yii::t('app','Specialist'),
];
?>
<div class="lavozimlar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'cat')->dropDownList($mylavozim, ['prompt' => ' -- Танланг --']); ?>




    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
