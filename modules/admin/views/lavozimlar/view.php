<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Lavozimlar */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lavozimlars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="lavozimlar-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name_uz',
            'name_ru',
            'name_en',
            ['attribute' => 'cat', 'label'=>Yii::t('app',"Catigorylavozim"),

                'format' => 'raw',
                'value' => function ($model) {
                    $active = $model->cat ;
                    $mylavozim=[
                        '1'=>Yii::t('app','University administration'),
                        '2'=>Yii::t('app','Directors and deans'),
                        '3'=>Yii::t('app','Heads of department'),
                        '4'=>Yii::t('app','Professors'),
                        '5'=>Yii::t('app','Associate professors'),
                        '6'=>Yii::t('app','Assistants and Teachers'),
                        '7'=>Yii::t('app','Heads of office'),
                        '8'=>Yii::t('app','Researchers'),
                        '9'=>Yii::t('app','Specialist'),
                    ];
                    return $mylavozim[$active];
                },
            ],
        ],
    ]) ?>

</div>
