<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Postlang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="postlang-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'posts_id')->textInput() ?>

    <?= $form->field($model, 'language')->radioList(Yii::$app->params['mylanguages']);?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'full_text')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
