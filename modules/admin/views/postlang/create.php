<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Postlang */

$this->title = Yii::t('app', 'Create Postlang');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Postlangs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="postlang-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,

    ]) ?>

</div>
