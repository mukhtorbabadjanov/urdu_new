<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAssetAdmin;
use yii\helpers\Url;

AppAssetAdmin::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php $this->registerCsrfMetaTags() ?>
    <title>Admin Panel</title>
    <script src="/assets/js/wow.js"></script>
    <?php $this->head() ?>
</head>
<body class="sticky-header" >
<?php $this->beginBody() ?>
<section >
    <!-- left side start-->
    <div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo">
            <a style="font-size: 14px; margin: 10px 0px;" href="/">Admin Panel</a>
        </div>
        <div class="logo-icon text-center">
            <a href="/"><i class="fa fa-home"></i> </a>
        </div>

        <!--logo and iconic logo end-->
        <div class="left-side-inner">

            <!--sidebar nav start-->



            <ul class="nav nav-pills nav-stacked custom-nav">


                <li ><a href="/admin/post"><i class="fa fa-book" aria-hidden="true"></i><span><?=Yii::t('app','News')?></span></a></li>
                <li><a href="/admin/hodim"><i class="fa fa-user" aria-hidden="true"></i><span>Hodimlar</span></a></li>
                <li><a href="/admin/center"><i class="fa fa-centos" aria-hidden="true"></i><span>Center</span></a></li>
                <li><a href="/admin/photogallery"><i class="fa fa-centos" aria-hidden="true"></i><span>Photo Gallery</span></a></li>
                <li><a href="/"><i class="fa fa-plus" aria-hidden="true"></i><span>Item1</span></a></li>
                <li><a href="/"><i class="fa fa-map-marker" aria-hidden="true"></i><span>Item1</span></a></li>
                <li class="menu-list">
                    <a href="#"><i class="fa fa-cog"></i>
                        <span><?=Yii::t('app','Config')?></span></a>
                    <ul class="sub-menu-list">
                        <li><a href="/admin/lavozimlar"><i class="fa fa-list-ol" aria-hidden="true"></i><span><?=Yii::t('app','lavozimlar')?></span></a></li>
                        <li><a href="/admin/catigory"><i class="fa fa-list" aria-hidden="true"></i><span>Kategoriyalar</span></a></li>
                        <li ><a href="/admin/user/index"><i class="fa fa-users"></i><span>Foydalanuvchilar</span></a></li>
                    </ul>
                </li>



            </ul>
            <!--sidebar nav end-->
        </div>
    </div>
    <!-- left side end-->
    <!-- main content start-->
    <div class="main-content">

        <!-- header-starts -->
        <div class="header-section">

            <!--toggle button start-->
            <a class="toggle-btn"><i class="fa fa-bars"></i></a>
            <!--toggle button end-->

            <div class="menu-right">
                <div class="user-panel-top">
                    <div class="profile_details_left">



                        <?php  if (Yii::$app->user->isGuest): ?>
                            <a class="text-white p-1" href="/site/signup">Sign up</a> <a class="text-white p-1" href="/site/login">Login</a>
                        <?php else:
                            echo Html::beginForm(['/site/logout'], 'post');
                            echo   Html::submitButton('Logout (' . Yii::$app->user->identity->username . ')',   ['class' => 'btn  p-1']);
                            echo Html::endForm();


                        endif;?>

                    </div>



                </div>
            </div>
        </div>

        <!-- //header-ends -->
        <div class="page-main" >

            <?= Alert::widget() ?>


                <?=
                Breadcrumbs::widget([
                    'homeLink'=>[
                        'label'=>Yii::t('yii','Home'),
                        'url'=>Yii::$app->homeUrl,
                    ],
                    'links'=>isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs']:[],
                ]);
                ?>

            <?php
            foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
                echo "<div class='alert alert-$key alert-dismissible fade show' role='alert' id='myalert'>
        $message
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
        <span aria-hidden='true'>&times;</span>
        </button>
        </div>";}
            ?>

            <?= $content ?>

        </div>
        <!--body wrapper end-->
    </div>


    <!-- main content end-->
</section>

<footer class="footer mt-auto bg-dark">
    <div class="container p-2 text-white">
        <div class="row ">
            <span class="text-white  ">© 2020 - <?=Yii::t('app', 'Urgench State University');?></span>
        </div>
        </div>

    </div>
</footer>


<?php
$script = <<< JS
        
   $(function() {
   $('.alert').delay(2000).fadeOut('slow');
    });
        
JS;

$this->registerJs($script);
?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
