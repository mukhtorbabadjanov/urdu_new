<?php

use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = Yii::t('app', 'Update', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'news'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="post-update">

    <h1 class="text-uppercase"><?= Html::encode($this->title) ?></h1>
    <?php if (!empty($model->image)) {
        printf("<p><img src='%s' style='max-width: 400px;'></p>", $model->image);


    }

    ?>



  <?= $this->render('_form', [
        'model' => $model,
        'status' =>'update',
        'modelpostlang' =>$modelpostlang,

    ])
  ?>



</div>
