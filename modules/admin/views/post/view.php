<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'news'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'creat_at',
            ['attribute' => 'user_id', 'label'=>Yii::t('app',"User"),


                'format' => 'raw',
                'value' => function ($model, $column) {
                    $active = $model->user_id ;
                    return \app\models\User::findById(['id'=>$active])['username'];
                },
            ],

            'image',
            'updated_at:datetime',


            ['attribute' => 'allow', 'label'=>Yii::t('app',"Allow"),
                'format' => 'raw',
                'value' => function ($model) {
                  return  $model->allow ? "<i class='text-info fa fa-unlock'></i>": "<i class='text-danger fa fa-lock'></i>";

                },
            ],

        ],
    ]);

 foreach ($modelitems as $items)
   echo  DetailView::widget([
         'model' => $items,
         'attributes' => [
             'id',
             ['attribute' => 'language', 'label'=>Yii::t('app',"Language"),


                 'format' => 'raw',
                 'value' => function ($model) {
                     $active = $model->language;
                     return Yii::$app->params['mylanguages'][$active];
                 },
             ],

             'title',
             'short_text',
             'full_text',

         ],
     ]);


    ?>



</div>
