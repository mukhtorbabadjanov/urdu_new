<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'news');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">

    <div class="card">
        <div class="card-body">

            <h1 class="text-uppercase"><?= Html::encode($this->title) ?></h1>

            <p>
                <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?=

            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'summary' => '', // скрыть
                'pager' => ['class' => '\yii\bootstrap4\LinkPager'],

                'columns' => [


                    ['attribute' => 'id', 'label' => 'ID', 'headerOptions' => ['width' => 15]],
                    ['attribute' => 'creat_at', 'label' => Yii::t('app', "creat_at"),
                        'headerOptions' => ['width' => 200],
                        'format' => 'datetime',

                    ],


                    ['attribute' => 'user_id', 'label' => Yii::t('app', "User"),
                        'headerOptions' => ['width' => 150],
                        'filter' => \app\models\User::findAllUsers(),
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $column) {
                            $active = $model->{$column->attribute};
                            return \app\models\User::findById(['id' => $active])['username'];
                        },
                    ],
                    //   'image',
                    //  'updated_at:datetime',

                    ['attribute' => 'cat_id', 'label' => Yii::t('app', "Catigory"),

                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $column) {
                            $active = $model->{$column->attribute};
                            return \app\models\Catigory::findById(['id' => $active])['name_uz'];
                        },
                    ],
                    ['attribute' => 'allow', 'label' => Yii::t('app', "Home"), 'format' => 'raw',
                        'filter' => [0 => "Yo'q", 1 => "Ha"], 'headerOptions' => ['width' => 150],
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $column) {
                            $active = $model->{$column->attribute};
                            if ($active) {
                                return "<i class='text-info fa fa-unlock'></i>";
                            } else {
                                return "<i class='text-danger fa fa-lock'></i>";
                            }
                        },
                    ],


                    ['class' => 'yii\grid\ActionColumn'],

                ],
            ]); ?>

            <?php Pjax::end();

            ?>
        </div>
    </div>
</div>
