<?php
mihaildev\elfinder\Assets::noConflict($this);
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\models\Search;
use yii\bootstrap4\Modal;
use dosamigos\tinymce\TinyMce;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\web\JsExpression;
\mihaildev\elfinder\Assets::register($this);
\mihaildev\elfinder\Assets::addLangFile(\Yii::$app->language, $this);
\mihaildev\elfinder\Assets::noConflict($this);
$searchform = new Search();
$mymenu = $searchform->myCatParentById();

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="post-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
        'id' => 'post-form'
    ]); ?>

    <?php

     echo $form->field($model, 'image')->widget(\mihaildev\elfinder\InputFile::className(), [
         'language'      => Yii::$app->language,
         'controller'    => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
         'path' => 'public', // будет открыта папка из настроек контроллера с добавлением указанной под деритории
         'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
         'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
         'options'       => ['class' => 'form-control'],
         'buttonOptions' => ['class' => 'btn btn-info'],
         'multiple'      => false       // возможность выбора нескольких файлов
     ]);

    ?>

    <?= $form->field($model, 'cat_id')->dropDownList($mymenu, ['prompt' => ' -- Танланг --']); ?>



    <div id="isop">
        <?php
        if ($status === 'update'){
        foreach (Yii::$app->params['mylanguages'] as $language => $items) {
           if (in_array($language, $modelpostlang)) {

               printf("<fieldset class='border p-2  form%s'  ><legend class='w-auto'> %s </legend>", $language, $items);

                   $csr=Yii::$app->request->csrfParam;
                   Modal::begin([
                       'title' => '<small>'.Yii::t('app',"Attention").'</small>',
                       'toggleButton' => ['label' => "<i class='fa fa-trash-o'></i>", 'class' => 'btn btn-sm btn-danger', 'title' => Yii::t('app', 'Delete')],
                       'headerOptions' => ['class' => 'bg-danger p-1'],
                       'id' => 'modaldelete'.$language,
                       'size' => 'modal-md',
                     //  'footerOptions' => ['class' => 'p-1'],
                       'footer' => "<button type='button' class='btn btn-sm btn-danger' data-dismiss='modal'>".Yii::t('app','No')."</button>
                   <a href='/admin/post/deletepostlang?id=".$model->translate($language)->id." type='button' class='btn btn-sm btn-info' >".Yii::t('app','Yes')."</a>",
                   ]);
                   echo "<div id='modalContent' class='text-center'>".Yii::t('app','Are you sure you want to delete this item?')."</div>";



                   Modal::end();


               } else
               printf("<fieldset class='border p-2  form%s'  ><legend class='w-auto'> %s </legend> 
                             <button class='btn btn-outline-warning' id='%s'><i class='fa fa-cut'></i></button> 
                             ", $language, $items, $language);

                echo $form->field($model->translate($language), "[$language]title")->textInput();

                echo $form->field($model->translate($language), "[$language]short_text")->textarea();

                echo $form->field($model->translate($language), "[$language]full_text")->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions(['elfinder', 'path' => 'files/public'],[]),
                ]);
                echo "</fieldset>";

        };};
        if ($status === 'create'){
            foreach (Yii::$app->params['mylanguages'] as $language => $items) {
                printf("<fieldset class='border p-2  form%s'  ><legend class='w-auto'> %s </legend> 
                          <button class='btn btn-outline-warning' id='%s'><i class='fa fa-cut'></i></button>  
                            ", $language, $items, $language);

                echo $form->field($model->translate($language), "[$language]title")->textInput();

                echo $form->field($model->translate($language), "[$language]short_text")->textarea();
                echo $form->field($model->translate($language), "[$language]full_text")->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions(['elfinder', 'path' => 'files/public'],[]),
                ]);
                echo "</fieldset>";
            };};
        ?>
    </div>
    <?= $form->field($model, 'allow')->checkbox(['value' => '1']) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
$script = <<< JS
    $('#uz').click(function () {
        $('#isop .formuz').remove(); 
});
    $('#ru').click(function () {
        $('#isop .formru').remove(); 
});
    $('#en').click(function () {
        $('#isop .formen').remove(); 
});
JS;
$this->registerJs($script);
?>
