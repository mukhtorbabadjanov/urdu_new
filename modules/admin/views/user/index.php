<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>


    <h1 class="text-uppercase"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);

     ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'', // скрыть
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute' => 'id', 'label'=>'ID', 'headerOptions' => ['width' => 30]],
            'username',
           // 'auth_key',
           // 'password_hash',
          //  'password_reset_token',
           'email:email',
            //'verification_token',
            ['attribute' => 'status', 'label'=>Yii::t('app','Status'),
                'filter' =>\app\models\User::getStatusList(), 'headerOptions' => ['width' => 150],
                'value' => function ($model, $key, $index, $column) {
                    $active = $model->{$column->attribute} ;
                    return  \app\models\User::getStatusList()[$active];
                },
                ],


            'created_at:datetime',
            'updated_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>


