<?php

use yii\helpers\Html;
//use yii\bootstrap4\ActiveForm;
use yii\widgets\ActiveForm;
use kartik\password\PasswordInput;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>



    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(
        ['placeholder' => Yii::t('app', 'Фойдаланувчи логинини киритинг'), 'autofocus' => true, 'maxlength' => true]) ?>

    <?php if ($model->scenario === 'create'): ?>

        <?= $form->field($model, 'password')->widget(PasswordInput::classname(),
            ['options' => ['placeholder' => Yii::t('app', 'Паролни киритинг')]]) ?>

    <?php else: ?>

        <?= $form->field($model, 'password')->widget(PasswordInput::classname(),
            ['options' => ['placeholder' => Yii::t('app', 'Change password ( if you want )')]]) ?>

    <?php endif ?>



    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'status')->dropDownList($model->statusList) ?>
    <?= $form->field($model, 'roles')->dropDownList($model->rolesList,['prompt' => ' -- Tanlang --']) ?>



    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
