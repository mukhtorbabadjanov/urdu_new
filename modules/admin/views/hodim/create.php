<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Hodim */

$this->title = 'Create Hodim';
$this->params['breadcrumbs'][] = ['label' => 'Hodims', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hodim-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
