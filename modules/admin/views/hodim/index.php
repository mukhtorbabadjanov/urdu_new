<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HodimSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hodims';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">

    <div class="card">
        <div class="card-body">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                <?= Html::a('Create Hodim', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pager' => ['class' => '\yii\bootstrap4\LinkPager'],
                'summary' => "",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    'name_uz',
                    //'name_ru',
                    //'name_en',
                    'lav_uz',
                    //'lav_ru',
                    //'lav_en',
                    'ilm_uz',
                    //'ilm_ru',
                    //'ilm_en',
                    'ilm1_uz',
                    //'ilm1_ru',
                    //'ilm1_en',
                    //'tel',
                    'email:email',
                    //'text_uz:ntext',
                    //'text_ru:ntext',
                    //'text_en:ntext',
                    //'cate',
                    //'img',
                    //'scholar:ntext',
                    //'lav_id',
                    //'cat',
                    //'GS:ntext',
                    //'Publications:ntext',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>

</div>
