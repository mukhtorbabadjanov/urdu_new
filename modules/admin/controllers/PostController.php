<?php

namespace app\modules\admin\controllers;

use app\models\PostLang;
use app\models\User;
use creocoder\translateable\TranslateableBehavior;
use Yii;
use app\models\Post;
use app\models\PostSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\validators\ValidationAsset;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    public $layout = 'main';
    public $enableCsrfValidation = true;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'deleteimage' => ['POST'],
                    'deletepostlang' => ['GET'],
                    'addform' => ['GET'],
                ],
            ],
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['language', 'title', 'fill_text', 'short_text'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelitems' => $this->findModel($id)->items,
        ]);
    }



    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Post();
        $model->creat_at = date("Y-m-d: H-m-s");
        $model->user_id = Yii::$app->user->identity->getId();
        if ($model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelpostlang = PostLang::find()->select('language')->where(['posts_id'=>$id])->orderBy(['language'=>SORT_DESC])->asArray()->all();
        $modelpostlang= \yii\helpers\ArrayHelper::getColumn($modelpostlang, 'language');

        $model->updated_at = date("Y-m-d: H-m-s");
      if ($model->load(Yii::$app->request->post()) && $model->save()) {
          $status='success'; $message=Yii::t('app', 'The record was successfully updated');

          Yii::$app->session->setFlash($status, $message);
            return $this->redirect(['view', 'id' => $model->id]);

        }
        return $this->render('update', [
            'model' => $model,
            'modelpostlang' =>$modelpostlang,
        ]);
    }

    public function actionDeleteimage($id)
    {
        $model = $this->findModel($id);
        $pathimg = Yii::$app->basePath . '/uploads/images/posts/' . $model->image;
        if ($model->image != null && file_exists($pathimg)) {

            unlink($pathimg);
            $model->image = null;
            $model->save();
        };
        return $this->redirect(['update', 'id' => $model->id]);
    }

    public function actionDeletepostlang($id)
    {
        if (($postlang = Postlang::findOne($id)) !== null) {
            $myid=$postlang->posts_id;
            $model = $this->findModel($myid);
            $postlang->delete();
            $status='success'; $message=Yii::t('app', 'The record was successfully deleted');

            Yii::$app->session->setFlash($status, $message);
        }
        return $this->redirect(['update', 'id' => $model->id]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $status='danger'; $message=Yii::t('app', 'The record was successfully deleted');

        Yii::$app->session->setFlash($status, $message);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
