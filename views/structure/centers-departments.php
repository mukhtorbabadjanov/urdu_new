<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Events';
?>
<h5>Centers and Departments</h5>
<hr class="bg-primary">
<div class="mt-3">
    <h4>Axborot texnologiyalar markazi</h4>


    <div class="row mb-5">
        <div class="col-sm-5  rounded-left">
            <div class="card-block text-center text-white">
                <img class="w-100 h-100" src="/assets/images/rector.jpg" alt="rektor">
            </div>
        </div>
        <div class="col-sm-7 bg-white rounded-right">
            <h5 class="text-center">Polvanov Sultonmahmud Baxadirovich</h5>
            <p class="mt-2 text-center">ATM bo'lim boshlig'i</p>

            <div class="row">
                <!--                        <div class="col-sm-6">-->
                <!--                            <p class="font-weight-bold">Pedagogik faoliyat sohasi: </p>-->
                <!--                            <p class="text-muted">Matematik analiz, funksional analiz, kompleks o‘zgaruvchili funksiyalar nazariyasi</p>-->
                <!--                        </div>-->
                <!--                        <div class="col-sm-6">-->
                <!--                            <p class="font-weight-bold">Ilmiy sohalardagi qiziqishlari: </p>-->
                <!--                            <p class="text-muted">Funksiyalarni analitik davom qildirish</p>-->
                <!--                        </div>-->
                <div class="col-sm-6">
                    <p class="font-weight-bold">Email:</p>
                    <p class=" text-muted">maxmud@urdu.uz</p>
                </div>
                <div class="col-sm-6">
                    <p class="font-weight-bold">Phone:</p>
                    <p class="text-muted">+998939226578</p>
                </div>
            </div>
        </div>
    </div>


    <hr>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#about">Markaz haqida</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#staff">Hodimlar</a>
        </li>


    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div id="about" class="container tab-pane active"><br>
            <h3>About</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
        <div id="staff" class="container tab-pane fade"><br>
            <h3>Xodimlar</h3>
            <div class="row mb-5">
                <div class="col-sm-5  rounded-left">
                    <div class="card-block text-center text-white">
                        <img class="w-100 h-100" src="/assets/images/rector.jpg" alt="rektor">
                    </div>
                </div>
                <div class="col-sm-7 bg-white rounded-right">
                    <h5 class="text-center">Polvanov Sultonmahmud Baxadirovich</h5>
                    <p class="mt-2 text-center">ATM bo'lim boshlig'i</p>

                    <div class="row">
<!--                        <div class="col-sm-6">-->
<!--                            <p class="font-weight-bold">Pedagogik faoliyat sohasi: </p>-->
<!--                            <p class="text-muted">Matematik analiz, funksional analiz, kompleks o‘zgaruvchili funksiyalar nazariyasi</p>-->
<!--                        </div>-->
<!--                        <div class="col-sm-6">-->
<!--                            <p class="font-weight-bold">Ilmiy sohalardagi qiziqishlari: </p>-->
<!--                            <p class="text-muted">Funksiyalarni analitik davom qildirish</p>-->
<!--                        </div>-->
                        <div class="col-sm-6">
                            <p class="font-weight-bold">Email:</p>
                            <p class=" text-muted">maxmud@urdu.uz</p>
                        </div>
                        <div class="col-sm-6">
                            <p class="font-weight-bold">Phone:</p>
                            <p class="text-muted">+998939226578</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


