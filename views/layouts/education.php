<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100 ">
<?= $this->render('_head') ?>
<body class="d-flex flex-column h-100 ">
<?php $this->beginBody() ?>

<?= $this->render('_menu') ?>

<div class="wrap">
	<?php
	/*
	NavBar::begin([
		'brandLabel' => Yii::$app->name,
		'brandUrl' => Yii::$app->homeUrl,
		'options' => [
			'class' => 'navbar-inverse navbar-fixed-top',
		],
	]);
	echo Nav::widget([
		'options' => ['class' => 'navbar-nav navbar-right'],
		'items' => [
			['label' => 'Home', 'url' => ['/site/index']],
			['label' => 'About', 'url' => ['/site/about']],
			['label' => 'Contact', 'url' => ['/site/contact']],
			Yii::$app->user->isGuest ? (
				['label' => 'Login', 'url' => ['/site/login']]
			) : (
				'<li>'
				. Html::beginForm(['/site/logout'], 'post')
				. Html::submitButton(
					'Logout (' . Yii::$app->user->identity->username . ')',
					['class' => 'btn btn-link logout']
				)
				. Html::endForm()
				. '</li>'
			)
		],
	]);
	NavBar::end();
	*/
	?>

	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<?= Alert::widget() ?>

		<h4 class="mt-3 text-primary">EDUCATION</h4>
		<div class="mb-3">
			<div class="row">
				<div class="col-md-3 mt-3 mb-3">
					<ul class="list-group list-group-flush">
						<a href="#innovation-in-education" data-toggle="collapse" class="list-group-item
						list-group-item-action
					">Innovation in Education</a>

						<ul id="innovation-in-education" class="collapse list-group">
							<a class="list-group-item list-group-item-info" href="">Protect based Learning</a>
							<a class="list-group-item list-group-item-info" href="">Center of Elite
								Education</a>
						</ul>

						<a href="#bachelors-degree" data-toggle="collapse" class="list-group-item
						list-group-item-action">Bachelor's & Specialist Degree Programs</a>
						
						<ul id="bachelors-degree" class="collapse list-group">
							<a class="list-group-item list-group-item-info" href="">Bakalavr talim yo'nalsihlari</a>
						</ul>

						<a href="<?= Url::to(['/education/masters-degree'])
						?>" class="list-group-item
						list-group-item-action">Master's Degree Programs</a>

						<a href="<?= Url::to(['/education/english-taught'])
						?>" class="list-group-item
						list-group-item-action">English Taught Programs</a>

						<a href="<?= Url::to(['/education/phd-degree'])
						?>" class="list-group-item
						list-group-item-action">Ph.D. Degree Programs</a>

						<a href="<?= Url::to(['/education/foundation-program'])
						?>" class="list-group-item
						list-group-item-action">Foundation Programs</a>

						<a href="<?= Url::to(['/education/distance-education'])
						?>" class="list-group-item
						list-group-item-action">Distance Education</a>

					</ul>
				</div>

				<div class="col-md-9 mt-3 mb-3">
					<?= $content ?>
				</div>
			</div>
		</div>
	</div>

</div>

<?= $this->render('_footer') ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
