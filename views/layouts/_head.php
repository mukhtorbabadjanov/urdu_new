<?php
use yii\helpers\Html;
?>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Urgench state University, UrSU, UrDU">
    <link rel="canonical" href="<?=Yii::$app->getHomeUrl()?>"/>
    <link rel="shortlink" href="<?=Yii::$app->getHomeUrl()?>"/>
    <link rel="apple-touch-icon" href="/assets/images/logo.png">
    <link rel="apple-touch-icon" sizes="70x70" href="<?=Yii::$app->getHomeUrl()?>/assets/images/logo.png">
    <link rel="apple-touch-icon" sizes="512x512" href="<?=Yii::$app->getHomeUrl()?>/assets/images/logo-512x512.png">
    <link rel="apple-touch-icon" sizes="192x192" href="<?=Yii::$app->getHomeUrl()?>/assets/images/logo-192x192.png">
    <link rel="apple-touch-icon" sizes="32x32" href="<?=Yii::$app->getHomeUrl()?>/assets/images/logo-32x32.png">
    <link rel="apple-touch-icon" sizes="16x16" href="<?=Yii::$app->getHomeUrl()?>/assets/images/logo-16x16.png">
    <link rel="manifest" href="<?=Yii::$app->getHomeUrl()?>/assets/css/manifest.webmanifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#317EFB"/>
    <meta name="author" content="UrSU SE GROUP">
    <meta name="Keywords" content="Urgench state University, UrSU, UrDU"/>
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="/assets/js/wow.js"></script>
    <?php $this->head() ?>
</head>