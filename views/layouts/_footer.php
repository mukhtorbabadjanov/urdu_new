<footer class="footer mt-auto bg-dark">
    <div class="container p-5 text-white">
        <div class="row ">
            <div class="col-md-4 col-sm-4 ">
                <div class="mb-3 text-center ">
                    <a href="https://urdu.uz">
                        <img src="/assets/images/logo.png" class="m-1" alt="Urgench state university">
                    </a>
                    <p class="mb-3 text-uppercase "><?=Yii::t('app', 'Urgench State University');?></p>
                </div>

            </div>
            <div class="col-md-4 col-sm-8">

                <address>
                    <p class="mb-4">ADDRESS</p>
                    14, Kh.Alimdjan, Urgench, 220100, Uzbekistan<br>
                    <i class="fa fa-phone"></i> +998 62 224 6700<br>
                    <i class="fa fa-envelope"></i> info@urdu.uz<br>
                    <i class="fa fa-bus"></i> Bus 7, 13, 28<br>

                </address>

            </div>


            <div class="col-md-4  col-sm-8">


                <p class="mb-4 mbr-fonts-style foot-title display-7">
                    CONNECT WITH US
                </p>
                <div class=" nav navbar  ">
                    <div class="nav-item">
                        <a class="text-white" href="https://twitter.com/ursu_uz" target="_blank" rel="noopener"><i
                                class="fab fa-twitter"></i></a>
                    </div>
                    <div class="nav-item">
                        <a class="text-white" href="https://www.facebook.com/urduuz" target="_blank" rel="noopener"><i
                                class="fab fa-facebook"></i></a>
                    </div>
                    <div class="nav-item">
                        <a class="text-white" href="https://www.youtube.com/channel/UCkQ8N0jtKR9VigB0FALuFvA"
                           target="_blank" rel="noopener"><i class="fab fa-youtube"></i></a>
                    </div>
                    <div class="nav-item">
                        <a class="text-white" href="https://instagram.com/urdumatbuotxizmati" target="_blank"
                           rel="noopener"><i class="fab fa-instagram"></i></a>
                    </div>
                    <div class="nav-item">
                        <a class="text-white" href="https://plus.google.com" target="_blank" rel="noopener"><i
                                class="fab fa-google-plus-g"></i></a>
                    </div>
                    <div class="nav-item">
                        <a class="text-white" href="https://t.me/urdu_pressa" target="_blank" rel="noopener"><i
                                class="fab
                        fa-telegram"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <span class="text-white  ">© 2020 - <?=Yii::t('app', 'Urgench State University');?></span>
    </div>
</footer>
<div class="scroll-up" style="display: block;">
    <a href="#" id="#home"><i class="fa fa-angle-up"></i></a>
</div>