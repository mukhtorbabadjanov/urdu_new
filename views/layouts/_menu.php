<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<header class="bg-dark">
    <div class="container d-flex flex-column flex-md-row align-items-center px-md-4 ">
        <div class="mr-md-auto  "></div>
        <nav class="nav nav-underline  ">
            <a class="text-white p-1" href="https://www.facebook.com/urduuz"><i class="p-2 fab fa-facebook"></i></a>
            <a class="text-white p-1" href="https://instagram.com/urdumatbuotxizmati"><i class="p-2 fab fa-instagram"></i></a>
            <a class="text-white p-1" href="https://t.me/urdu_pressa"><i class="p-2 fab fa-telegram-plane"></i></a>
            <a class="text-white p-1" href="https://twitter.com/ursu_uz"><i class="p-2 fab fa-twitter"></i></a>
            <a class="text-white p-1" href="#"><i class="p-2 fab fa-linkedin"></i></a>

        </nav>
        <?= \cinghie\multilanguage\widgets\MultiLanguageWidget::widget([
            // 'addCurrentLang' => true, // add current lang
            'calling_controller' => $this->context,
            'image_type'  => 'classic', // classic or rounded
            //'link_home'   => true, // true or false
            'widget_type' => 'classic', // classic or selector
            'width'       => '25'
        ]); ?>

        <?php  if (Yii::$app->user->isGuest): ?>
            <a class="text-white p-1" href="/site/signup">Sign up</a> <a class="text-white p-1" href="/site/login">Login</a>
        <?php else:
            echo Html::beginForm(['/site/logout'], 'post');
            echo   Html::submitButton('Logout (' . Yii::$app->user->identity->username . ')',   ['class' => 'btn text-white p-1']);
            echo Html::endForm();


        endif;?>
    </div>


</header>


    <header id="sticky" >
        <nav class="bg-white navbar main-header navbar-expand-lg ">
            <div class="container">

                <div class="navbar-brand mr-md-auto" >
                    <a class="p-0  d-flex flex-column  flex-md-wrap align-items-center" href="<?=Yii::$app->homeUrl?>">
                        <img src="<?=Yii::$app->request->baseUrl?>/assets/images/logo.png" class="mr-1" alt="Urgench state university!"/>
                        <h6 class="my-0  text-dark text-uppercase " ><?=Yii::t('app', 'Urgench State University')?></h6>
                    </a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarsExample07XL">
                    <div class="mr-auto"></div>
                    <ul class="navbar-nav my-2 my-md-0">
                        <li class="nav-item ">
                            <a class="nav-link" href="<?= Url::to(['/about/index'])
                            ?>">ABOUT </a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link  dropdown-toggle" href="#" id="dropdown07XL"
                               data-toggle="dropdown" aria-expanded="false">EDUCATION</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown07XL">
                                <a class="dropdown-item" href="<?= Url::to(['/education/innovation-in-education'])
                                ?>">Innovation in Education</a>
                                <a class="dropdown-item" href="<?= Url::to(['/education/bachelors-degree'])
                                ?>">Bachelor's & Specialist Degree Programs</a>
                                <a class="dropdown-item" href="<?= Url::to(['/education/masters-degree'])
                                ?>">Master's Degree Programs</a>
                                <a class="dropdown-item" href="<?= Url::to(['/education/english-taught'])
                                ?>">English-Taught Programs</a>
                                <a class="dropdown-item" href="<?= Url::to(['/education/phd-degree'])
                                ?>">Ph.D. Degree Programs</a>
                                <a class="dropdown-item" href="<?= Url::to(['/education/foundation-program'])
                                ?>">Foundation Programs</a>
                                <a class="dropdown-item" href="<?= Url::to(['/education/distance-education'])
                                ?>">Distance Education</a>
                            </div>
                        </li>

                        <li class="nav-item text-uppercase">
                            <a class="nav-link  " href="<?= Url::to(['/interrelationship/projects'])
                            ?>"> INTERNATIONAL RELATIONSHIP </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link  dropdown-toggle" href="#" id="dropdown07XL"
                               data-toggle="dropdown" aria-expanded="false">SCIENTIFIC ACTIVITY</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown07XL">
                                <a class="dropdown-item" href="<?= Url::to(['/scientificactivity/p-fundamental'])
                                ?>">Istiqbolli loyihalar</a>
                                <a class="dropdown-item" href="<?= Url::to(['/scientificactivity/e-partnership'])
                                ?>">Ta'lim, fan integratsiyasi</a>
                                <a class="dropdown-item" href="<?= Url::to(['/scientificactivity/on-innovation'])
                                ?>">Amalga oshirilayotgan loyihalar</a>
                                <a class="dropdown-item" href="<?= Url::to(['/scientificactivity/c-international'])
                                ?>">Konferensiya va seminarlar</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link  dropdown-toggle" href="#" id="dropdown07XL"
                               data-toggle="dropdown" aria-expanded="false">ADMISSION</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown07XL">
                                <a class="dropdown-item" href="<?= Url::to(['admission/admissions'])
                                ?>">Admissions 2021</a>
                                <a class="dropdown-item" href="<?= Url::to(['admission/why-ursu'])
                                ?>">Why UrSU</a>
                                <a class="dropdown-item" href="<?= Url::to(['admission/admission-trajectory'])
                                ?>">Admission Trajectory</a>
                                <a class="dropdown-item" href="<?= Url::to(['admission/government-scholarship'])
                                ?>">Government Scholarship</a>
                                <a class="dropdown-item" href="<?= Url::to(['admission/when-you-arrive'])
                                ?>">When you arrive</a>
                                <a class="dropdown-item" href="<?= Url::to(['admission/student-visa'])
                                ?>">Student Visa</a>
                                <a class="dropdown-item" href="<?= Url::to(['admission/important-information'])
                                ?>">Important Information</a>
                            </div>
                        </li>


                    </ul>
                </div>

            </div>
        </nav>
    </header>

