<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Urgench state University, UrSU, UrDU">
	<link rel="canonical" href="https://urdu.uz"/>
	<link rel="shortlink" href="https://www.urdu.uz"/>
	<link rel="apple-touch-icon" href="assets/images/logo.png">
	<link rel="apple-touch-icon" sizes="70x70" href="assets/images/logo.png">
	<link rel="apple-touch-icon" sizes="512x512" href="assets/images/logo-512x512.png">
	<link rel="apple-touch-icon" sizes="192x192" href="assets/images/logo-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/images/logo-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/images/logo-16x16.png">
	<link rel="manifest" href="assets//css/manifest.webmanifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#317EFB"/>
	<meta name="author" content="UrSU SE GROUP">
	<meta name="Keywords" content="Urgench state University, UrSU, UrDU"/>
	<?php $this->registerCsrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>

	<script src="/assets/js/wow.js"></script>

	<?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<?=$this->render('_menu')?>

<div class="wrap">
	<?php
	/*
	NavBar::begin([
		'brandLabel' => Yii::$app->name,
		'brandUrl' => Yii::$app->homeUrl,
		'options' => [
			'class' => 'navbar-inverse navbar-fixed-top',
		],
	]);
	echo Nav::widget([
		'options' => ['class' => 'navbar-nav navbar-right'],
		'items' => [
			['label' => 'Home', 'url' => ['/site/index']],
			['label' => 'About', 'url' => ['/site/about']],
			['label' => 'Contact', 'url' => ['/site/contact']],
			Yii::$app->user->isGuest ? (
				['label' => 'Login', 'url' => ['/site/login']]
			) : (
				'<li>'
				. Html::beginForm(['/site/logout'], 'post')
				. Html::submitButton(
					'Logout (' . Yii::$app->user->identity->username . ')',
					['class' => 'btn btn-link logout']
				)
				. Html::endForm()
				. '</li>'
			)
		],
	]);
	NavBar::end();
	*/
	?>

	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<?= Alert::widget() ?>

		<h4 class="mt-3 text-primary">INTERNATIONAL RELATIONSHIP</h4>
		<div class="mb-3">
			<div class="row">
				<div class="col-md-3 mb-3 mt-3">
					<div class="list-group list-group-flush">
						<a href="#" class="list-group-item list-group-item-action active">Xalqaro loyiha va grantlar</a>
						<a href="#" class="list-group-item list-group-item-action">Xorijda malaka oshirish stajirovka va
							xizmat safarlari</a>
						<a href="#" class="list-group-item list-group-item-action">Xorijiy o'qituvchilar</a>
						<a href="#" class="list-group-item list-group-item-action">Xorijiy talabalar</a>
						<a href="#" class="list-group-item list-group-item-action">Tuzilma</a>
						<a href="#" class="list-group-item list-group-item-action">Xalqaro reytinglarda ishtirok etish</a>
						<a href="#" class="list-group-item list-group-item-action">Xalqaro hamkorlar</a>
						<a href="#" class="list-group-item list-group-item-action">Xalqaro strategik reja</a>
						<a href="#" class="list-group-item list-group-item-action">Scopus</a>
						<a href="#" class="list-group-item list-group-item-action">MCOLE</a>
					</div>

				</div>

				<div class="col-md-9 mt-3 mb-3">
					<?= $content ?>
				</div>
			</div>
		</div>
	</div>

</div>


<footer class="footer mt-auto bg-dark">
	<div class="container p-5 text-white">
		<div class="row ">
			<div class="col-md-4 col-sm-4 ">
				<div class="mb-3 text-center ">
					<a href="https://urdu.uz">
						<img src="/assets/images/logo.png" class="m-1" alt="Urgench state university">
					</a>
					<p class="mb-3 text-uppercase ">Urgench state university</p>
				</div>

			</div>
			<div class="col-md-4 col-sm-8">

				<address>
					<p class="mb-4">ADDRESS</p>
					14, Kh.Alimdjan, Urgench, 220100, Uzbekistan<br>
					<i class="fa fa-phone"></i> +998 62 224 6700<br>
					<i class="fa fa-envelope"></i> info@urdu.uz<br>
					<i class="fa fa-bus"></i> Bus 7, 13, 28<br>

				</address>

			</div>


			<div class="col-md-4  col-sm-8">


				<p class="mb-4 mbr-fonts-style foot-title display-7">
					CONNECT WITH US
				</p>
				<div class=" nav navbar  ">
					<div class="nav-item">
						<a class="text-white" href="https://twitter.com" target="_blank" rel="noopener"><i class="fab fa-twitter"></i></a>
					</div>
					<div class="nav-item">
						<a class="text-white" href="https://www.facebook.com/urduuz" target="_blank" rel="noopener"><i
									class="fab fa-facebook"></i></a>
					</div>
					<div class="nav-item">
						<a class="text-white" href="https://www.youtube.com/channel/UCkQ8N0jtKR9VigB0FALuFvA" target="_blank" rel="noopener"><i class="fab fa-youtube"></i></a>
					</div>
					<div class="nav-item">
						<a class="text-white" href="https://instagram.com/urdumatbuotxizmati" target="_blank"
						   rel="noopener"><i class="fab fa-instagram"></i></a>
					</div>
					<div class="nav-item">
						<a class="text-white" href="https://plus.google.com" target="_blank" rel="noopener"><i class="fab fa-google-plus-g"></i></a>
					</div>
					<div class="nav-item">
						<a class="text-white" href="https://t.me/urdu_pressa" target="_blank" rel="noopener"><i
									class="fab
                        fa-telegram"></i></a>
					</div>
				</div>
			</div>
		</div>
		<span class="text-white  ">© 2020 Urgench state university</span>
	</div>
</footer>
<div class="scroll-up" style="display: block;">
	<a href="#" id="#home"><i class="fa fa-angle-up"></i></a>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
