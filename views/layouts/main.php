<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Urgench state University, UrSU, UrDU">
	<link rel="canonical" href="<?=Yii::$app->getHomeUrl()?><?=Yii::$app->request->url?>"/>
	<link rel="shortlink" href="<?=Yii::$app->getHomeUrl()?><?=Yii::$app->request->url?>"/>
	<link rel="apple-touch-icon" href="/assets/images/logo.png">
	<link rel="apple-touch-icon" sizes="70x70" href="<?=Yii::$app->getHomeUrl()?>/assets/images/logo.png">
	<link rel="apple-touch-icon" sizes="512x512" href="<?=Yii::$app->getHomeUrl()?>/assets/images/logo-512x512.png">
	<link rel="apple-touch-icon" sizes="192x192" href="<?=Yii::$app->getHomeUrl()?>/assets/images/logo-192x192.png">
	<link rel="apple-touch-icon" sizes="32x32" href="<?=Yii::$app->getHomeUrl()?>/assets/images/logo-32x32.png">
	<link rel="apple-touch-icon" sizes="16x16" href="<?=Yii::$app->getHomeUrl()?>/assets/images/logo-16x16.png">
	<link rel="manifest" href="<?=Yii::$app->getHomeUrl()?>/assets/css/manifest.webmanifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#317EFB"/>
	<meta name="author" content="UrSU SE GROUP">
	<meta name="Keywords" content="Urgench state University, UrSU, UrDU"/>
	<?php $this->registerCsrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<script src="/assets/js/wow.js"></script>
	<?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100" >
<?php $this->beginBody() ?>
<main role="main" class="flex-shrink-0" >
<?= $this->render('_menu') ?>


<div class="wrap bg-fon" >


	<div class="container">

		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<?= Alert::widget() ?>

	</div>
	<?= $content ?>
</div>
</main>

<?= $this->render('_footer') ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
