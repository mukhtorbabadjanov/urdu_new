<?php

/* @var $this yii\web\View */

$this->title = 'Rectorate';
?>

<h5>Rectorate</h5>
<hr class="bg-primary">
<div class="container">
	<div class="row mb-5">
		<div class="col-sm-5 rounded-left">
			<div class="card-block text-center text-white">
				<img class="h-100 w-100" src="/assets/images/rector.jpg" alt="rektor">
			</div>
		</div>
		<div class="col-sm-7 bg-white rounded-right">
			<h5 class="text-center">Abdullayev Baxrom Ismoilovich</h5>
			<p class="mt-2 text-center">Rektor</p>

			<div class="row">
				<div class="col-sm-6">
					<p class="font-weight-bold">Adress:</p>
					<p class="text-muted">Xorazm viloyati Urgench shahar H.Olimjon ko'chasi 14 uy</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Qabul:</p>
					<p class="text-muted"> Har kuni 08:00-11:00</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Email:</p>
					<p class=" text-muted">rektor@urdu.uz</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Phone:</p>
					<p class="text-muted">(998-62)224-6700</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-sm-5  rounded-left">
			<div class="card-block text-center text-white">
				<img class="w-100 h-100" src="/assets/images/rector.jpg" alt="rektor">
			</div>
		</div>
		<div class="col-sm-7 bg-white rounded-right">
			<h5 class="text-center">Xujaniyazov Sardor Umarovich</h5>
			<p class="mt-2 text-center">O'quv ishlari bo'yicha prorektor</p>
			<div class="row">
				<div class="col-sm-6">
					<p class="font-weight-bold">Adress:</p>
					<p class=" text-muted">Xorazm viloyati Urgench shahar H.Olimjon ko'chasi 14 uy</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Qabul:</p>
					<p class="text-muted"> Seshanba, 15:00-18:00, Juma, 15:00-18:00</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Email:</p>
					<p class=" text-muted"> sardor@urdu.uz</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Phone:</p>
					<p class="text-muted">8-362-224-6620</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-sm-5  rounded-left">
			<div class="card-block text-center text-white">
				<img class="w-100 h-100" src="/assets/images/rector.jpg" alt="rektor">
			</div>
		</div>
		<div class="col-sm-7 bg-white rounded-right">
			<h5 class="text-center">Ibragimov Zafar Shavkatovich</h5>
			<p class="mt-2 text-center">Ilmiy ishlar va innovatsiyalar bo`yicha prorektor</p>
			<div class="row">
				<div class="col-sm-6">
					<p class="font-weight-bold">Adress:</p>
					<p class=" text-muted">Xorazm viloyati Urgench shahar H.Olimjon ko'chasi 14 uy</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Qabul:</p>
					<p class="text-muted"> Chorshanba, 15:00-18:00.</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Email:</p>
					<p class=" text-muted">zafar@urdu.uz</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Phone:</p>
					<p class="text-muted">(998-62)224-6621</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-sm-5  rounded-left">
			<div class="card-block text-center text-white">
				<img class="w-100 h-100" src="/assets/images/rector.jpg" alt="rektor">
			</div>
		</div>
		<div class="col-sm-7 bg-white rounded-right">
			<h5 class="text-center">Davletov Sanjarbek Rajabovich</h5>
			<p class="mt-2 text-center">Yoshlar bilan ishlash bo'yicha prorektor</p>
			<div class="row">
				<div class="col-sm-6">
					<p class="font-weight-bold">Adress:</p>
					<p class=" text-muted">Xorazm viloyati Urgench shahar H.Olimjon ko'chasi 14 uy</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Qabul:</p>
					<p class="text-muted"> Dushanba, 15:00-18:00</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Email:</p>
					<p class=" text-muted"> pr-mm-urdu@umail.uz</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Phone:</p>
					<p class="text-muted">(+998-62)224-6668</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-sm-5  rounded-left">
			<div class="card-block text-center text-white">
				<img class="w-100 h-100" src="/assets/images/rector.jpg" alt="rektor">
			</div>
		</div>
		<div class="col-sm-7 bg-white rounded-right">
			<h5 class="text-center">Atajanov Alisher Omonboyevich</h5>
			<p class="mt-2 text-center">Moliya va iqtisodiyot ishlari bo`yicha prorektor</p>
			<div class="row">
				<div class="col-sm-6">
					<p class="font-weight-bold">Adress:</p>
					<p class=" text-muted">Xorazm viloyati Urgench shahar H.Olimjon ko'chasi 14 uy</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Qabul:</p>
					<p class="text-muted"> Juma, 15:00-18:00.</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Email:</p>
					<p class=" text-muted"> alisheratajanov@urdu.uz</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Phone:</p>
					<p class="text-muted">(998-62)224-66-88</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-sm-5  rounded-left">
			<div class="card-block text-center text-white">
				<img class="w-100 h-100" src="/assets/images/rector.jpg" alt="rektor">
			</div>
		</div>
		<div class="col-sm-7 bg-white rounded-right">
			<h5 class="text-center">O‘razboyev G‘ayrat O‘razaliyevich</h5>
			<p class="mt-2 text-center">Xalqaro hamkorlik bo‘yicha prorektor</p>
			<div class="row">
				<div class="col-sm-6">
					<p class="font-weight-bold">Adress:</p>
					<p class=" text-muted">Xorazm viloyati Urgench shahar H.Olimjon ko'chasi 14 uy</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Qabul:</p>
					<p class="text-muted"> Payshanba, 14:00-16:00</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Email:</p>
					<p class=" text-muted"> gayrat71@mail.ru</p>
				</div>
				<div class="col-sm-6">
					<p class="font-weight-bold">Phone:</p>
					<p class="text-muted">(998-62)224-66-11</p>
				</div>
			</div>
		</div>
	</div>


</div>