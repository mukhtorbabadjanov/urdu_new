<?php

/* @var $this yii\web\View */

$this->title = 'History';
?>

<h5>History of University</h5>
<hr class="bg-primary">
<p class="text-justify">Urganch davlat universiteti ilgaridan faoliyat ko'rsatib kelgan Xorazm davlat pedagogika
	instituti
	asosida tashkil topdi. Xorazm Davlat pedagogika instituti 1 sentyabr 1942 yilda to'liqsiz oliy
	ma'lumotli o'qituvchilar tayyorlaydigan institut asosida tashkil topgan. Institut tarkibida 4 ta
	fakultet mavjud edi: fizika-matematika, o'zbek tili va adabiyoti, rus tili va adabiyoti, tarix. 1991
	yilga kelib XDPI da 6 ta fakultet va 22 kafedra faoliyat olib borgan.

	Urganch davlat universiteti O'zbekiston Respublikasi Prezidentining 1992 yil 28 fevraldagi PF-356 sonli
	farmoniga muvofiq, oldingi Xorazm davlat pedagogika instituti negizida tashkil qilingan.

	Urganch davlat universitetida 11 ta fakultet, 36 ta kafedra faoliyat ko'rsatib, ularda 58 ta'lim
	yo'nalishlari bo'yicha bakalavrlar, 27 ta mutaxassislik bo'yicha magistrlar tayyorlanmoqda. Talabalar
	kontingenti bakalavriat kunduzgi bo'limida 16496 nafar.

	Urganch davlat universiteti "Erasmus+" ning ECMUM, RENES, GREB, CLASS, EPCA kabi dasturlarida aktiv
	ishtirok etadi.</p>
