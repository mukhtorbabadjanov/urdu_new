<?php

/* @var $this yii\web\View */

$this->title = 'Financial Activity';
?>

<h5>Financial Activity</h5>
<hr class="bg-primary">

<h6>Buxgalteriya</h6>
<p class="font-weight-bold">Urganch davlat universiteti “Buxgalteriya” boʼlimi faoliyati</p>
<p class="text-justify">Universitet hisob raqamlarida pul mablagʼlari aylanishi jarayoni, asosiy vositalar harakati,
	daromad va xarajatlarni maqsadli rejalashtirish va majburiyatlarni bajarishni oʼz vaqtida aks ettirish; <br>
	Universitet faoliyatidan kelib chaqadigan soliqlar va boshqa majburiy toʼlovlarni Respublika byudjeti, mintaqaviy va
	mahalliy byudjetlarga, byudjetdan tashqari ijtimoiy fondlarga, kredit tashkilotlari va moliyalashtirish obʼektlariga
	oʼz vaqtida oʼtkazib berish; <br>
	Urganch davlat universiteti tomonidan mehnat uchun haq toʼlash fondining sarflanishini va ishchilarga haq toʼlash
	tizimini toʼgʼri olib borilishini, asosiy vositalarni inventarizatsiyadan oʼtkazishni, buxgalteriya hisobi va
	hisobotlarni yuborish tartibini nazorat qilish; <br>
	Tashkilot ichki auditida buxgalteriya hisobi va hisobotlaridagi maʼlumotlar asosida moliyaviy taxlil oʼtkazishda va
	soliq siyosatini shakllantirishda ishtirok etish; <br>
	Moliyaviy va gʼazna tartib qoidalariga rioya etishni taʼminlash, yetishmovchiliklarning, debitor va kreditor
	qarzdorliklarning buxgalteriya xisobi hisobotlaridan chiqarishni qonuniyligini taʼminlash boʼyicha ishlar olib
	borish.</p>

<p class="font-weight-bold">Urganch davlat universiteti faoliyati bilan bogliq moliyaviy xujjatlar</p>
