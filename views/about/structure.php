<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Structure';
?>

<h5>Structure</h5>
<hr class="bg-primary">

<h6 class="text-primary">Rahbariyat</h6>
<div class="list-group mb-4">
    <a href="<?= Url::to(['about/rectorate']) ?>" class="list-group-item list-group-item-action">Rektorat</a>
    <a href="#" class="list-group-item list-group-item-action">Universitet kengashi</a>
</div>

<h6 class="text-primary">Fakultetlar</h6>
<div class="list-group mb-4">
    <a href="<?= Url::to(['structure/faculties']) ?>" class="list-group-item
	list-group-item-action">Fizika-matematika</a>
    <a href="#" class="list-group-item list-group-item-action">Xorijiy-filologiya</a>
    <a href="#" class="list-group-item list-group-item-action">Filologiya</a>
    <a href="#" class="list-group-item list-group-item-action">Tabiiy fanlar</a>
    <a href="#" class="list-group-item list-group-item-action">Pedagogika</a>
    <a href="#" class="list-group-item list-group-item-action">Jismoniy madaniyat</a>
    <a href="#" class="list-group-item list-group-item-action">Tarix</a>
    <a href="#" class="list-group-item list-group-item-action">Turizm va iqtisodiyot</a>
    <a href="#" class="list-group-item list-group-item-action">Texnika</a>
    <a href="#" class="list-group-item list-group-item-action">Kimyoviy texnologiyalar</a>
    <a href="#" class="list-group-item list-group-item-action">San'atshunoslik</a>
    <a href="#" class="list-group-item list-group-item-action">Bioinjeneriya va oziq-ovqat xavfsizligi</a>
</div>
<h6 class="text-primary">Kafedra</h6>
<div class="list-group mb-4">
    <a href="<?= Url::to(['cafedra/cafedra']) ?>" class="list-group-item
	list-group-item-action">Kafedralar</a>

</div>
<h6 class="text-primary">Markaz va bo'limlar</h6>
<div class="list-group mb-4">
    <a href="<?= Url::to(['structure/centers-departments']) ?>" class="list-group-item list-group-item-action">Axborot
        texnologiyalari markazi</a>
    <a href="#" class="list-group-item list-group-item-action">Axborot resurs markazi</a>
    <a href="#" class="list-group-item list-group-item-action">Ta'lim sifatini nazorat qilish bo'limi</a>
    <a href="#" class="list-group-item list-group-item-action">Ilmiy tadqiqotlar, innovatsiyalar va ilmiy-pedagogik
        kadrlar tayyorlash bo'limi</a>
    <a href="#" class="list-group-item list-group-item-action">O'quv uslubiy boshqarma</a>
    <a href="#" class="list-group-item list-group-item-action">Ichki nazorat monitoring bo'limi</a>
    <a href="#" class="list-group-item list-group-item-action">Magistratura bo'limi</a>
    <a href="#" class="list-group-item list-group-item-action">Xodimlar bo'limi</a>
    <a href="#" class="list-group-item list-group-item-action">Ma'naviyat-ma'rifat bo'limi</a>
    <a href="#" class="list-group-item list-group-item-action">Iqtidorli talabalar bilan ishlash bo'limi</a>
    <a href="#" class="list-group-item list-group-item-action">Innovatsion jamg'arma</a>
    <a href="#" class="list-group-item list-group-item-action">Yuristkonsult bo'limi</a>
    <a href="#" class="list-group-item list-group-item-action">Xalqaro a'loqalar bo'limi</a>
    <a href="#" class="list-group-item list-group-item-action">Xalqaro reyting bo'yicha ishlash bo'limi</a>
    <a href="#" class="list-group-item list-group-item-action">Kasaba uyushma qo'mitasi bo'limi</a>
    <a href="#" class="list-group-item list-group-item-action">Buxgalteriya</a>
    <a href="#" class="list-group-item list-group-item-action">Ilmiy kengash</a>
</div>