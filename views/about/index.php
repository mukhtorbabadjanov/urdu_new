<?php

/* @var $this yii\web\View */

$this->title = 'About';
?>

<h5>Universitet haqida</h5>
<hr class="bg-primary">
<p class="text-justify">The university was originally organized in 1942 as the Khorezm State Pedagogical Institute. In
	1991—2017, the University was named after mathematician and philosopher Al-Khorezmi (alternative transliteration
	Muhammad ibn Musa al-Khwarizmi) as Urgench State University after named Al-Khorezmi. In 2017, the Urgench State
	University after named Al-Khorezmi was finally renamed the Urgench State University, a name it keeps to date. The
	rector of the university since 2016 is Dr. Bakhrom Ismoilovich Abdullayev. At present, there are 12 faculties and 39
	departments. The university provides 74 Bachelor programs; 27 Master programs; 39 Doctoral programs, 5 joint
	bachelor programs; 6 joint master programs. The total number of teaching staff working at the university is 826,
	including 211 doctors of sciences (DSC), professors and PhDs.</p>
