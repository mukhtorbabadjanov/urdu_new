<?php

/* @var $this yii\web\View */

$this->title = 'News';
?>
<h5>Latest News</h5>
<hr class="bg-primary">

<div class="album">
	<div class="container news-page">

		<div class="row">
			<div class="col-md-6">
				<div class="card mb-4 box-shadow">
					<img class="card-img-top" src="<?=Yii::$app->request->baseUrl?>/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
					<div class="card-body">
						<h5>
							<a class="text-dark" href="#">Post title</a>
						</h5>
						<p class="card-text text-justify">This is a wider card with supporting text below as a natural
							lead-in to	additional content. This content is a little bit longer.</p>
						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-outline-secondary">More</button>
							</div>
							<small class="text-muted"><i class="fas fa-eye"></i> 3000</small>
							<small class="text-muted">admin <i class="fas fa-calendar-alt"></i> 2021-11-12</small>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="card mb-4 box-shadow">
					<img class="card-img-top" src="<?=Yii::$app->request->baseUrl?>/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
					<div class="card-body">
						<h5>
							<a class="text-dark" href="#">Post title</a>
						</h5>
						<p class="card-text text-justify">This is a wider card with supporting text below as a natural
							lead-in to	additional content. This content is a little bit longer.</p>
						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-outline-secondary">More</button>
							</div>
							<small class="text-muted"><i class="fas fa-eye"></i> 3000</small>
							<small class="text-muted">admin <i class="fas fa-calendar-alt"></i> 2021-11-12</small>
						</div>
					</div>
				</div>
			</div>


			<div class="col-md-6">
				<div class="card mb-4 box-shadow">
					<img class="card-img-top" src="<?=Yii::$app->request->baseUrl?>/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
					<div class="card-body">
						<h5>
							<a class="text-dark" href="#">Post title</a>
						</h5>
						<p class="card-text text-justify">This is a wider card with supporting text below as a natural
							lead-in to	additional content. This content is a little bit longer.</p>
						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-outline-secondary">More</button>
							</div>
							<small class="text-muted"><i class="fas fa-eye"></i> 3000</small>
							<small class="text-muted">admin <i class="fas fa-calendar-alt"></i> 2021-11-12</small>
						</div>
					</div>
				</div>
			</div>


			<div class="col-md-6">
				<div class="card mb-4 box-shadow">
					<img class="card-img-top" src="<?=Yii::$app->request->baseUrl?>/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
					<div class="card-body">
						<h5>
							<a class="text-dark" href="#">Post title</a>
						</h5>
						<p class="card-text text-justify">This is a wider card with supporting text below as a natural
							lead-in to	additional content. This content is a little bit longer.</p>
						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-outline-secondary">More</button>
							</div>
							<small class="text-muted"><i class="fas fa-eye"></i> 3000</small>
							<small class="text-muted">admin <i class="fas fa-calendar-alt"></i> 2021-11-12</small>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="card mb-4 box-shadow">
					<img class="card-img-top" src="<?=Yii::$app->request->baseUrl?>/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
					<div class="card-body">
						<h5>
							<a class="text-dark" href="#">Post title</a>
						</h5>
						<p class="card-text text-justify">This is a wider card with supporting text below as a natural
							lead-in to	additional content. This content is a little bit longer.</p>
						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-outline-secondary">More</button>
							</div>
							<small class="text-muted"><i class="fas fa-eye"></i> 3000</small>
							<small class="text-muted">admin <i class="fas fa-calendar-alt"></i> 2021-11-12</small>
						</div>
					</div>
				</div>
			</div>


			<div class="col-md-6">
				<div class="card mb-4 box-shadow">
					<img class="card-img-top" src="<?=Yii::$app->request->baseUrl?>/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
					<div class="card-body">
						<h5>
							<a class="text-dark" href="#">Post title</a>
						</h5>
						<p class="card-text text-justify">This is a wider card with supporting text below as a natural
							lead-in to	additional content. This content is a little bit longer.</p>
						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-outline-secondary">More</button>
							</div>
							<small class="text-muted"><i class="fas fa-eye"></i> 3000</small>
							<small class="text-muted">admin <i class="fas fa-calendar-alt"></i> 2021-11-12</small>
						</div>
					</div>
				</div>
			</div>


			<div class="col-md-6">
				<div class="card mb-4 box-shadow">
					<img class="card-img-top" src="<?=Yii::$app->request->baseUrl?>/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
					<div class="card-body">
						<h5>
							<a class="text-dark" href="#">Post title</a>
						</h5>
						<p class="card-text text-justify">This is a wider card with supporting text below as a natural
							lead-in to	additional content. This content is a little bit longer.</p>
						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group">
								<button type="button" class="btn btn-sm btn-outline-secondary">More</button>
							</div>
							<small class="text-muted"><i class="fas fa-eye"></i> 3000</small>
							<small class="text-muted">admin <i class="fas fa-calendar-alt"></i> 2021-11-12</small>
						</div>
					</div>
				</div>
			</div>




		</div>
	</div>
</div>

