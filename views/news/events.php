<?php

/* @var $this yii\web\View */

$this->title = 'Events';
?>
<h5>Events</h5>
<hr class="bg-primary">
<div class="row mb-2 event-page">
	<div class="col-md-12">
		<div class="card flex-md-row mb-4 box-shadow h-md-250">
			<div class="card-body d-flex flex-column align-items-start">
				<strong class="d-inline-block mb-2 text-success">Design</strong>
				<h3 class="mb-0">
					<a class="text-dark" href="#">Post title</a>
				</h3>
				<div class="mb-1 text-muted">Nov 11</div>
				<p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
				<a href="#">Continue reading</a>
			</div>
			<img class="card-img-right flex-auto d-none d-md-block" width="400" height="250"
			     src="<?=Yii::$app->request->baseUrl?>/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
		</div>
	</div>

	<div class="col-md-12">
		<div class="card flex-md-row mb-4 box-shadow h-md-250">
			<div class="card-body d-flex flex-column align-items-start">
				<strong class="d-inline-block mb-2 text-success">Design</strong>
				<h3 class="mb-0">
					<a class="text-dark" href="#">Post title</a>
				</h3>
				<div class="mb-1 text-muted">Nov 11</div>
				<p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
				<a href="#">Continue reading</a>
			</div>
			<img class="card-img-right flex-auto d-none d-md-block" width="400" height="250"
			     src="<?=Yii::$app->request->baseUrl?>/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
		</div>
	</div>

	<div class="col-md-12">
		<div class="card flex-md-row mb-4 box-shadow h-md-250">
			<div class="card-body d-flex flex-column align-items-start">
				<strong class="d-inline-block mb-2 text-success">Design</strong>
				<h3 class="mb-0">
					<a class="text-dark" href="#">Post title</a>
				</h3>
				<div class="mb-1 text-muted">Nov 11</div>
				<p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
				<a href="#">Continue reading</a>
			</div>
			<img class="card-img-right flex-auto d-none d-md-block" width="400" height="250"
			     src="<?=Yii::$app->request->baseUrl?>/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
		</div>
	</div>

	<div class="col-md-12">
		<div class="card flex-md-row mb-4 box-shadow h-md-250">
			<div class="card-body d-flex flex-column align-items-start">
				<strong class="d-inline-block mb-2 text-success">Design</strong>
				<h3 class="mb-0">
					<a class="text-dark" href="#">Post title</a>
				</h3>
				<div class="mb-1 text-muted">Nov 11</div>
				<p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
				<a href="#">Continue reading</a>
			</div>
			<img class="card-img-right flex-auto d-none d-md-block" width="400" height="250"
			     src="<?=Yii::$app->request->baseUrl?>/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
		</div>
	</div>

	<div class="col-md-12">
		<div class="card flex-md-row mb-4 box-shadow h-md-250">
			<div class="card-body d-flex flex-column align-items-start">
				<strong class="d-inline-block mb-2 text-success">Design</strong>
				<h3 class="mb-0">
					<a class="text-dark" href="#">Post title</a>
				</h3>
				<div class="mb-1 text-muted">Nov 11</div>
				<p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
				<a href="#">Continue reading</a>
			</div>
			<img class="card-img-right flex-auto d-none d-md-block" width="400" height="250"
			     src="<?=Yii::$app->request->baseUrl?>/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
		</div>
	</div>

	<div class="col-md-12">
		<div class="card flex-md-row mb-4 box-shadow h-md-250">
			<div class="card-body d-flex flex-column align-items-start">
				<strong class="d-inline-block mb-2 text-success">Design</strong>
				<h3 class="mb-0">
					<a class="text-dark" href="#">Post title</a>
				</h3>
				<div class="mb-1 text-muted">Nov 11</div>
				<p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
				<a href="#">Continue reading</a>
			</div>
			<img class="card-img-right flex-auto d-none d-md-block" width="400" height="250"
			     src="<?=Yii::$app->request->baseUrl?>/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
		</div>
	</div>

</div>


