<?php

/* @var $this yii\web\View */

$this->title = 'Photogallery';

?>
<h5>Photogallery</h5>
<hr class="bg-primary">

<div class="container news-page">
    <div class="row">
        <?php foreach ($gallery as $item) { ?>
            <div class="col-md-6">
                <div class="thumbnail">
                    <a href="<?= Yii::$app->request->baseUrl ?>/uploads/images/gallery/<?=$item->image?>"
                       target="_blank">
                        <img src="<?= Yii::$app->request->baseUrl ?>/uploads/images/gallery/thumb/<?=$item->image_cat?>"
                             alt="Lights" style="width:100%; height: 200px">
                        <div class="caption">
                            <p><?=$item->title_uz?></p>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>
    </div>
</div>


