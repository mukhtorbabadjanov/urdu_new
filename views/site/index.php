<?php
$this->title = Yii::t('app', 'Urgench State University');

use yii\helpers\Url;
use yii\helpers\Html;
use evgeniyrru\yii2slick\Slick;
use yii\web\JsExpression;

/* @var $this yii\web\View */

/*
echo yii\bootstrap4\Carousel::widget([
      'items' => [
         // the item contains only the image
          ['content' => '<img src="/assets/images/51F96233685BFC8B1301161433B62D80.jpg" style="object-fit: cover"/>',
      //      'options' => ['class'=>['screen-height']],
              ],
          // equivalent to the above
          ['content' => '<img src="/assets/images/814F06AB7F40B2CFF77F2C7BDFFD3415.jpg"/>',
         //     'options' => ['class'=>['screen-height']],
              ],
          // the item contains both the image and the caption
          [
              'content' => '<img src="/assets/images/814F06AB7F40B2CFF77F2C7BDFFD3415.jpg"/>',
              'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
        //      'captionOptions' => ['class' => ['d-none', 'd-md-block','text-left']],
       //       'options' => ['class'=>['screen-height'], 'style'=>['background-image: url(/assets/images/51F96233685BFC8B1301161433B62D80.jpg)', 'background-attachment: fixed', 'background-repeat: no-repeat', 'background-position: center',  'background-size: cover']],
          ],
      ]
  ]);
*/
?>

<div id="myCarousel" class="carousel slide mt-lg-1  " data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active screen-height"
             style="background-image: url(/assets/images/51F96233685BFC8B1301161433B62D80.jpg);  background-repeat: no-repeat; background-position: center;  background-size: cover;">
            <div class="container">
                <div class="carousel-caption text-left">
                    <h1>Example headline.</h1>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida
                        at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
                </div>
            </div>
        </div>
        <div class="carousel-item screen-height"
             style="background-image: url(/assets/images/814F06AB7F40B2CFF77F2C7BDFFD3415.jpg);  background-repeat: no-repeat; background-position: center;  background-size: cover;">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Another example headline.</h1>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida
                        at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
                </div>
            </div>
        </div>
        <div class="carousel-item screen-height"
             style="background-image: url(/assets/images/814F06AB7F40B2CFF77F2C7BDFFD3415.jpg);  background-repeat: no-repeat; background-position: center;  background-size: cover;">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Another example headline.</h1>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida
                        at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<nav class="nav nav-underline container">
    <a class="nav-link active" href="<?= Url::to(['staff/index']) ?>">Staff</a>
    <a class="nav-link" href="#">Friends <span class="badge badge-pill bg-light align-text-bottom">27</span></a>
    <a class="nav-link" href="#">Explore</a>
    <a class="nav-link" href="#">Suggestions</a>
    <a class="nav-link" href="#">5 ta muhim tashabbus</a>
    <a class="nav-link" href="#">COVID 19</a>
    <a class="nav-link" href="#">MCOLE</a>
    <a class="nav-link" href="#">Scopus</a>
    <a class="nav-link" href="#">Link</a>
</nav>
<div class="bg-fon" >
<div class="mb-lg-4 mt-lg-4" >
    <div class="container">
        <h1 class="display-4">Welcome!</h1>
        <p>
            The university was originally organized in 1942 as the Khorezm State Pedagogical Institute. In 1991—2017,
            the University was named after mathematician and philosopher Al-Khorezmi (alternative transliteration
            Muhammad ibn Musa al-Khwarizmi) as Urgench State University after named Al-Khorezmi.

            In 2017, the Urgench State University after named Al-Khorezmi was finally renamed the Urgench State
            University, a name it keeps to date. The rector of the university since 2016 is Dr. Bakhrom Ismoilovich
            Abdullayev.

            At present, there are 12 faculties and 39 departments. The university provides 74 Bachelor programs; 27
            Master programs; 39 Doctoral programs, 5 joint bachelor programs; 6 joint master programs. The total number
            of teaching staff working at the university is 826, including 211 doctors of sciences (DSC), professors and
            PhDs.</p>
        <p><a class="see-all" href="#" ><?=Yii::t('app','More')?></a></p>
    </div>
</div>
    <?php
   // print_r($posts);

    ?>

<div class="container news-card">
    <div class="mb-md-5 mt-md-5 border-bottom d-flex flex-column flex-md-row">
        <h4 class="text-uppercase mr-md-auto"><?=Yii::t('app','news')?></h4><a href="<?= Url::to(['news/news']) ?>"> <span class="see-all"><?=Yii::t('app','See all')?></span></a>
    </div>
    <div class="row row-cols-1 row-cols-md-3">
        <?php
            foreach ($posts as $key=>$items):
        ?>
        <div class="col mb-4">
            <div class="card">
               <img src="<?php if(empty($items['image'])) {echo "/uploads/images/posts/de4fac51c543ter6b66ea8f678a13d8cf.jpg";} else {echo $items['image'];}; ?>" class="bd-news-img card-img-top " alt="Urgench state university" style="height: 200px">
                <div class="card-body p-0 ">
                    <div class="text-muted d-flex flex-column flex-md-row p-1">
                        <small class="mr-md-auto">
                            <?= date('d-M-Y',strtotime($items['creat_at'])) ?>
                        </small>
                    </div>
                    <div class="card-text">
                        <a href="<?=Yii::$app->homeUrl.$items['url']."/".$items['id'] ?>"><?=$items['title'] ?></a>
                        <p><?=$items['short_text'] ?> </p>
                    </div>
                    <div class="text-muted">
                        <small>
                            <a href="<?=Yii::$app->homeUrl.$items['url'] ?>">
                                #<?=$items['name_'.Yii::$app->language] ?>
                            </a>
                        </small>

                    </div>
                </div>
            </div>
        </div>
        <?php
             endforeach;
        ?>
    </div>
</div>

    <div class="container mb-2 mt-md-5">
        <div class="border-bottom d-flex flex-column flex-md-row">
            <h3 class="mr-md-auto">СМИ О НАС</h3><a href="#"><span class="see-all"><?=Yii::t('app','See all')?></span></a>
        </div>
    </div>
    <div class="container" >
        <div class="card-columns">
            <div class="card movie-wrap">
                <a href="#">
                    <small class="fa fa-play"></small>
                </a>
                <img class="card-img-top" src="/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
            </div>

            <div class="card movie-wrap">
                <a href="#">
                    <small class="fa fa-play"></small>
                </a>
                <img class="card-img-top" src="/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
            </div>

            <div class="card movie-wrap">
                <a href="#">
                    <small class="fa fa-play"></small>
                </a>
                <img class="card-img-top" src="/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
            </div>

            <div class="card movie-wrap">
                <a href="#">
                    <small class="fa fa-play"></small>
                </a>
                <img class="card-img-top" src="/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
            </div>

            <div class="card movie-wrap">
                <a href="#">
                    <small class="fa fa-play"></small>
                </a>
                <img class="card-img-top" src="/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
            </div>

            <div class="card movie-wrap">
                <a href="#">
                    <small class="fa fa-play"></small>
                </a>
                <img class="card-img-top" src="/uploads/images/posts/3b73b73abbf53abb3a3b73f53bbf53.png" alt="Card image cap">
            </div>
        </div>
    </div>


    <div class="container mb-2 mt-2 mb-md-5 mt-md-5">
        <div class="border-bottom d-flex flex-column flex-md-row">
            <h3 class="mr-md-auto text-uppercase"><?=Yii::t('app','Events')?></h3>
            <a href="<?= Url::to(['news/events']) ?>"><span class="see-all"><?=Yii::t('app','See all')?></span></a>
        </div>
        <div class="row justify-content-center d-flex align-items-stretch">

            <div class="events col-md-5 col-lg-5 m-3 h-100">
                <div class="event-items">
                    <a href="#" class="event-items-link">
                        <div class="row">
                            <div class="event-items-content-date col-12 col-sm-3">
                                <span>Jan</span>
                                <span>31</span>
                            </div>
                            <div class="event-items-content-text col-sm-8 col-12">
                                <p class="mb-1">post.title</p>
                                <span class="mb-0">post.comment</span>
                            </div>
                            <div class="event-items-content-icon"><i class="fa fa-chevron-right"></i></div>
                        </div>
                    </a>
                </div>

            </div>

            <div class="events col-md-5 col-lg-5 m-3 h-100">
                <div class="event-items">
                    <a href="#" class="event-items-link">
                        <div class="row">
                            <div class="event-items-content-date col-12 col-sm-3">
                                <span>Jan</span>
                                <span>31</span>
                            </div>
                            <div class="event-items-content-text col-sm-8 col-12">
                                <p class="mb-1">post.title</p>
                                <span class="mb-0">post.comment</span>
                            </div>
                            <div class="event-items-content-icon"><i class="fa fa-chevron-right"></i></div>
                        </div>
                    </a>
                </div>

            </div>

            <div class="events col-md-5 col-lg-5 m-3 h-100">
                <div class="event-items">
                    <a href="#" class="event-items-link">
                        <div class="row">
                            <div class="event-items-content-date col-12 col-sm-3">
                                <span>Jan</span>
                                <span>31</span>
                            </div>
                            <div class="event-items-content-text col-sm-8 col-12">
                                <p class="mb-1">post.title</p>
                                <span class="mb-0">post.comment</span>
                            </div>
                            <div class="event-items-content-icon"><i class="fa fa-chevron-right"></i></div>
                        </div>
                    </a>
                </div>

            </div>

            <div class="events col-md-5 col-lg-5 m-3 h-100">
                <div class="event-items">
                    <a href="#" class="event-items-link">
                        <div class="row">
                            <div class="event-items-content-date col-12 col-sm-3">
                                <span>Jan</span>
                                <span>31</span>
                            </div>
                            <div class="event-items-content-text col-sm-8 col-12">
                                <p class="mb-1">post.title</p>
                                <span class="mb-0">post.comment</span>
                            </div>
                            <div class="event-items-content-icon"><i class="fa fa-chevron-right"></i></div>
                        </div>
                    </a>
                </div>

            </div>

        </div>
    </div>
    <div class="stats-bg">
        <h3 class="text-center pt-5 text-white">Asosiy ko'rsatkichlar</h3>
        <?=Slick::widget([

            // HTML tag for container. Div is default.
            'itemContainer' => 'div',


            // HTML attributes for widget container
            'containerOptions' => ['class' => 'container stats text-white pb-5 pt-5'],

            // Items for carousel. Empty array not allowed, exception will be throw, if empty
            'items' => [
                '<h4>Professor-o\'qituvchilar soni</h4><p>830</p>',
                '<h4>Talabalar</h4><p>23556</p>',
                '<h4>ARM Fondi</h4><p>1865000</p>',
                '<h4>Bakalavriat ta\'lim dasturlari</h4><p>58</p>',
                '<h4>Magistratura ta\'lim dasturlari</h4><p>27</p>',
                '<h4>Xalqaro talabalar soni</h4><p>1280</p>',


            ],

            // HTML attribute for every carousel item
            'itemOptions' => ['class' => 'text-center  p-5'],

            // settings for js plugin
            // @see http://kenwheeler.github.io/slick/#settings
            'clientOptions' => [
                'autoplay' => true,
                'dots'     => false,
                // note, that for params passing function you should use JsExpression object
                //  'onAfterChange' => new JsExpression('function() {console.log("The cat has shown")}'),
            ],

        ]); ?>
    </div>








    <div class="container mb-md-5 mt-md-5">
        <div class="card-columns">
            <div class="card">
                <img class="card-img-top" src="/assets/images/51F96233685BFC8B1301161433B62D80.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title that wraps to a new line</h5>
                    <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                </div>
            </div>
            <div class="card p-3">
                <blockquote class="blockquote mb-0 card-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <footer class="blockquote-footer">
                        <small class="text-muted">
                            Someone famous in <cite title="Source Title">Source Title</cite>
                        </small>
                    </footer>
                </blockquote>
            </div>
            <div class="card">
                <img class="card-img-top" src="/assets/images/51F96233685BFC8B1301161433B62D80.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
            <div class="card bg-primary text-white text-center p-3">
                <blockquote class="blockquote mb-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat.</p>
                    <footer class="blockquote-footer">
                        <small>
                            Someone famous in <cite title="Source Title">Source Title</cite>
                        </small>
                    </footer>
                </blockquote>
            </div>
            <div class="card text-center">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
            <div class="card">
                <img class="card-img" src="/assets/images/51F96233685BFC8B1301161433B62D80.jpg" alt="Card image">
            </div>
            <div class="card p-3 text-right">
                <blockquote class="blockquote mb-0">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <footer class="blockquote-footer">
                        <small class="text-muted">
                            Someone famous in <cite title="Source Title">Source Title</cite>
                        </small>
                    </footer>
                </blockquote>
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>


<div id="fon" class="screen-height" style="height: 500px;">
    <div class="footer-table footer-bg">
        <div class="footer-table-cell">
            <div class="video-wrap text-center">
                <a href="https://mover.uz/video/embed/uyVXF4Dm/" class=" wow pulse animated"
                   data-wow-iteration="infinite" data-wow-duration="500ms"
                   style="visibility: visible;-webkit-animation-duration: 500ms; -moz-animation-duration: 500ms; animation-duration: 500ms;-webkit-animation-iteration-count: infinite; -moz-animation-iteration-count: infinite; animation-iteration-count: infinite;"><i
                            class="fa fa-play"></i></a>
            </div>
        </div>
    </div>
</div>

</main>


