<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\bootstrap4\Alert;
$this->title = $name;

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <h1><?= Html::encode($this->title) ?></h1>
<?php  \yii\bootstrap4\Alert::begin(['options' => ['class' => 'alert-danger']]);
echo nl2br(Html::encode($message)) ;
Alert::end();
?>



    <p>
        The above error occurred while the Web server was processing your request.
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p>

</div>
