<?php


use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap4\LinkPager;
use yii\widgets\Breadcrumbs;
use kartik\select2\Select2;
use kartik\base\InputWidget;
use yii\web\JsExpression;
use app\models\Center;
use app\models\Hodim;
use app\models\Search;


$this->title = Yii::t('app', 'Urgench State University') . "-" . Yii::t('app', 'Employees and Teachers');

$this->params['breadcrumbs'][] = Yii::t('app', 'Employees and Teachers');
?>


<div class="container">

       <h3><?= Yii::t('app', 'Employees and Teachers') ?></h3>
    <div class="row">
<div class='col-md-8'>

    <?php
    $searchform = new Search();
    $center = $searchform->mydep();
    $lavozim = $searchform->mylavozim();
 //  print_r($model); exit();
    ?>

    <?php Pjax::begin(); ?>
    <div class="list-group mb-2 mt-2">
        <?php foreach ($model as $item) { ?>
<a href="<?= Url::to(['/staff/view', 'id' => $item['id']]) ?>" class="list-group-item list-group-item-action"><?= $item['name_' . Yii::$app->language] ?>
    <p class="mb-1"><?= $center[$item['cate']] ?>, <?= $lavozim[$item['lav_id']] ?></p>
</a>

        <?php } ?>
    </div>

    <?php echo LinkPager::widget(['pagination' => $pages]); ?>

    <?php Pjax::end(); ?>
</div>
<div class="col-md-4">


    <fieldset class='border p-2 bg-white '  >
        <legend class='w-auto'><small><i class="fa fa-search"></i> <?= Yii::t('app', 'Advanced Search') ?></small>  </legend>



            <?php
            $url = Url::to(['search']);
            $searchform = new Search();


            $form = ActiveForm::begin(['id' => 'search-form', 'method' => 'get']);

            echo $form->field($searchform, 'name')->widget(Select2::classname(), [

                'options' => ['placeholder' => Yii::t('app', 'Search employee'),],
                'pluginOptions' => [
                    'tags' => true,
                    'minimumInputLength' => 3,
                    'language' => ['errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(name) { return name.text; }'),
                    'templateSelection' => new JsExpression('function (name) { return name.text; }'),
                ],
            ]);
            ?>


            <?php

            echo $form->field($searchform, 'dep')->widget(Select2::classname(), [

                'data' => $searchform->mydep(),
                'options' => ['placeholder' => Yii::t('app', 'Select a department'),],
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],

                ],]);

            echo $form->field($searchform, 'filters')->checkboxList([
                '1' => Yii::t('app', 'University administration'),
                '2' => Yii::t('app', 'Directors and deans'),
                '3' => Yii::t('app', 'Heads of department'),
                '4' => Yii::t('app', 'Professors'),
                '5' => Yii::t('app', 'Associate professors'),
                '6' => Yii::t('app', 'Assistants and Teachers'),
                '7' => Yii::t('app', 'Heads of office'),
                '8' => Yii::t('app', 'Researchers'),
            ]);
            ?>

            <button class="btn btn-info btn-block"><i class="fa fa-search"></i> <?= Yii::t('app', 'Search') ?></button>


            <?php ActiveForm::end(); ?>

    </fieldset>


    </div>
</div>



	