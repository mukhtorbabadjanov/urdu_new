<?php
/**
 * @var $model Hodim
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use yii\bootstrap4\Breadcrumbs;
use kartik\select2\Select2;
use kartik\base\InputWidget;
use yii\web\JsExpression;
use app\models\Center;
use app\models\Hodim;
use app\models\Search;


$this->title = Yii::t('app', 'Urgench State University') . "-" . Yii::t('app', 'Employees and Teachers');


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Employees and Teachers'), 'url' => ['/staff/index'], 'template' => "<li class='breadcrumb-item'>{link}</li>"];
$this->params['breadcrumbs'][] = ['label' => 'Publactivity', 'template' => "<li class='breadcrumb-item'>{link}</li>"];

?>


<div class="container">

    <div class="page-header"><h2 class="h1-text"><?= Yii::t('app', 'Hodimlar') ?></h2></div>


    <div class="mt-3">

        <div class="row mb-5">
            <div class="col-sm-5  rounded-left">
                <div class="card-block text-center text-white">
                    <img class="w-100 h-100" src="<?= $model->img?>" alt="hodim">
                </div>
            </div>
            <div class="col-sm-7 bg-white rounded-right">
                <h5 class="text-center"><?=$model->name_uz?></h5>
                <p class="mt-2 text-center"><?=$model->lav_uz?></p>

                <p class="font-weight-bold">Pedagogik faoliyat sohasi: </p>
                <p class="text-muted"><?=$model->ilm1_uz?></p>

                <p class="font-weight-bold">Ilmiy sohalardagi qiziqishlari: </p>
                <p class="text-muted"><?=$model->ilm_uz?></p>

                <p class="font-weight-bold">Email:</p>
                <p class=" text-muted"><?=$model->email?></p>

                <p class="font-weight-bold">Phone:</p>
                <p class="text-muted"><?=$model->tel?></p>
            </div>

            </div>
        </div>


        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#about">Hodim haqida</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#publications">Nashrlar</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#schedule">Dars jadvali</a>
            </li>

        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div id="about" class="container tab-pane active"><br>
                <h3>Hodim haqida</h3>
                <p><?=$model->text_uz?></p>
            </div>
            <div id="publications" class="container tab-pane fade"><br>
                <h3>Nashrlar</h3>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>
            <div id="schedule" class="container tab-pane fade"><br>
                <h3>Dars jadvali</h3>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
            </div>

        </div>
    </div>

<!--<div class="container">-->
<!--    <main class="row">-->
<!--        <div class="col-md-4 border-right">-->
<!--            <div>-->
<!--                <img class="w-100" height="350" src="https://urdu.uz/hodimlar/1614837761.jpg" alt="Hodim">-->
<!--                <h3 class="cv-person_name text-primary text-center mt-2">Sarvarbek Rakhimov</h3>-->
<!--                <h6 class="cv-person_position text-center">Business management, team management, translation, public administrationr</h6>-->
<!--            </div>-->
<!--            <div class="mt-4">-->
<!--                <h4>Contact</h4>-->
<!--                <ul class="list-group-flush">-->
<!--                    <li class="list-group-item"><a href="tel: +998932410894" class="fas fa-phone"> +998932410894</a></li>-->
<!--                    <li class="list-group-item"><a href="mailto: sjrakhimov1808@gmail.com" class="fas fa-envelope">  sjrakhimov1808@gmail.com</a></li>-->
<!--                    <li class="list-group-item"><a href="#" class="fas fa-home">  Xorazm viloyati,Xiva tumani</a></li>-->
<!--                </ul>-->
<!--            </div>-->
<!--            <div>-->
<!--                <h4>Social Media</h4>-->
<!--                <ul class="list-group-flush">-->
<!--                    <li class="list-group-item"><a href="#" class="fab fa-telegram" target="_blank"> example</a></li>-->
<!--                    <li class="list-group-item"><a href="#" class="fab fa-twitter" target="_blank"> example</a></li>-->
<!--                    <li class="list-group-item"><a href="#" class="fab fa-facebook" target="_blank"> example</a></li>-->
<!--                    <li class="list-group-item"><a href="#" class="fab fa-instagram" target="_blank"> example</a></li>-->
<!--                </ul>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-md-8 cv-content">-->
<!--            <div class="border-bottom">-->
<!--                <h4>EDUCATION</h4>-->
<!--                <div>-->
<!--                    <h6>-->
<!--                        TERMEZ STATE-->
<!--                        UNIVERSITY-->
<!--                        <span>| 2013-2017</span>-->
<!--                    </h6>-->
<!--                    <ul>-->
<!--                        <li>BACHELOR IN ECONOMICS-->
<!--                            <ul>-->
<!--                                <li>The department of Economics (in branches and-->
<!--                                    spheres). GPA was 87% out of 100</li>-->
<!--                            </ul>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--                <div>-->
<!--                    <h6>-->
<!--                        INCHEON NATIONAL-->
<!--                        UNIVERSITY-->
<!--                        <span>| 2018-2020</span>-->
<!--                    </h6>-->
<!--                    <ul>-->
<!--                        <li>MASTER IN PUBLIC ADMINISTRATION-->
<!--                            <ul>-->
<!--                                <li>The graduate school of INU, department of-->
<!--                                    Public Administration. GPA, 4.38 out of 4.50</li>-->
<!--                            </ul>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!---->
<!--            <div class="border-bottom">-->
<!--                <h4 class="">EXPERIENCE</h4>-->
<!--                <div>-->
<!--                    <h6>-->
<!--                        UZUN SUMMER-->
<!--                        CAMP-->
<!--                        <span>|2014-2016</span>-->
<!--                    </h6>-->
<!--                    <ul>-->
<!--                        <li>Junior team manager for organizing and-->
<!--                            conducting events. Surkhandarya, Uzbekistan</li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--                <div>-->
<!--                    <h6>-->
<!--                        KHIVA DISTRICT-->
<!--                        ADMINISTRATION-->
<!--                        <span>|2016-2017</span>-->
<!--                    </h6>-->
<!--                    <ul>-->
<!--                        <li>Assistant manager for the branch of-->
<!--                            Economic and Tourism affairs. Khiva,-->
<!--                            Uzbekistan</li>-->
<!--                    </ul>-->
<!--                </div>-->
<!---->
<!--                <div>-->
<!--                    <h6>-->
<!--                        INCHEON NATIONAL-->
<!--                        UNIVERSITY-->
<!--                        <span>|2018-2020</span>-->
<!--                    </h6>-->
<!--                    <ul>-->
<!--                        <li>Research assistant for Dr. Jesse W Campbell.-->
<!--                            Department of Public Administration. INU,-->
<!--                            Songdo, South Korea</li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!---->
<!--            <div>-->
<!--                <h4>Skills</h4>-->
<!--                <div>-->
<!--                    <ul>-->
<!--                        <li>Speaking languages: Uzbek, Russian, English, Korean</li>-->
<!--                        <li>Microsoft office programs</li>-->
<!--                        <li>Stata Driving license</li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </main>-->
<!---->
<!--</div>-->