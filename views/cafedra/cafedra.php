<?php

/* @var $this yii\web\View */

$this->title = 'Kafedra';
?>
<h5>Kafedra</h5>
<hr class="bg-primary">
<div class="mt-3">
    <h4>Axborot texnologiyalari</h4>


    <div class="row mb-5">
        <div class="col-sm-5  rounded-left">
            <div class="card-block text-center text-white">
                <img class="w-100 h-100" src="/assets/images/rector.jpg" alt="rektor">
            </div>
        </div>
        <div class="col-sm-7 bg-white rounded-right">
            <h5 class="text-center">Madatov Xabibulla Axmedovich</h5>
            <p class="mt-2 text-center">Kafedra mudiri</p>

            <div class="row">
<!--                <div class="col-sm-6">-->
<!--                    <p class="font-weight-bold">Pedagogik faoliyat sohasi: </p>-->
<!--                    <p class="text-muted">Matematik analiz, funksional analiz, kompleks o‘zgaruvchili funksiyalar nazariyasi</p>-->
<!--                </div>-->
<!--                <div class="col-sm-6">-->
<!--                    <p class="font-weight-bold">Ilmiy sohalardagi qiziqishlari: </p>-->
<!--                    <p class="text-muted">Funksiyalarni analitik davom qildirish</p>-->
<!--                </div>-->
                <div class="col-sm-6">
                    <p class="font-weight-bold">Email:</p>
                    <p class=" text-muted"> khabibulla@urdu.uz</p>
                </div>
                <div class="col-sm-6">
                    <p class="font-weight-bold">Phone:</p>
                    <p class="text-muted">+998939223272</p>
                </div>
            </div>
        </div>
    </div>


    <hr>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#about">Kafedra haqida</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#news">Yangiliklar</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#works">Ilmiy ishlar</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#staff">Hodimlar</a>
        </li>

    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div id="about" class="container tab-pane active"><br>
            <h3>About</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
        <div id="news" class="container tab-pane fade"><br>
            <h3>News</h3>
            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        </div>
        <div id="works" class="container tab-pane fade"><br>
            <h3>Ilmiy ishlar</h3>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
        </div>

        <div id="staff" class="container tab-pane fade"><br>
            <h3>Hodimlar</h3>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
        </div>

    </div>
</div>


