<?php


namespace app\controllers;


use yii\web\Controller;

class AboutController extends Controller
{
	public $layout = 'about';
	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionHistory(){
		return $this->render('history');
	}

	public function actionBuildings(){
		return $this->render('buildings');
	}

	public function actionContact(){
		return $this->render('contact');
	}

	public function actionFinalAttestations(){
		return $this->render('final-attestations');
	}

	public function actionFinancialActivity(){
		return $this->render('financial-activity');
	}

	public function actionMainFacts(){
		return $this->render('main-facts');
	}

	public function actionNormativeDocuments(){
		return $this->render('normative-documents');
	}

	public function actionRectorate(){
		return $this->render('rectorate');
	}

	public function actionStructure(){
		return $this->render('structure');
	}

	public function actionStudentResidence(){
		return $this->render('student-residence');
	}

	public function actionUniversityCouncil(){
		return $this->render('university-council');
	}
}