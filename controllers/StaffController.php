<?php

namespace app\controllers;

use yii\web\Controller;
use Yii;
use app\models\Hodim;
use app\models\Center;
use yii\base\InvalidArgumentException;
use yii\data\Pagination;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

//use app\models\Center;
use yii\helpers\ArrayHelper;

class StaffController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //    'logout' => ['post'],
                    //	     'search' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        $this->layout = 'main';
     //   $model = Hodim::find()->orderBy('name_en asc');

        if (!isset($_GET['Search'])) {

            $model = Hodim::find()->orderBy('name_en asc');

        } else {

            $model = Hodim::find();
            if (!empty($_GET['Search']['name'])) {
                $model->where(['like', 'name_' . Yii::$app->language, '%' . $_GET['Search']['name'] . '%', false]);
                $model->orWhere(['id' => $_GET['Search']['name']]);
            };
            if (!empty($_GET['Search']['dep'])) {
                $model->andWhere(['cate' => $_GET['Search']['dep']]);
            };
            if (!empty($_GET['Search']['filters'])) {
                $model->andWhere(['cat' => $_GET['Search']['filters']]);
            };
            $model->orderBy('name_en asc');

            //   $resultsearch->mysearch(array('name_uz'=>'Anvar'));
            // print_r($resultsearch->mysearch(array('name_uz'=>'Anvar')));
        };
        $pages = new Pagination(['totalCount' => $model->count(), 'pageSize' => 20]);
        $pages->pageSizeParam = false;
        $model = $model->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('index', ['model' => $model, 'pages' => $pages,
        ]);
    }

    public function actionPublactivity()
    {
        if (isset($_POST['fakultet'])) {
            $myfakultet = $_POST['fakultet'];
            $kafedralar = ArrayHelper::map(Center::find()->select(['id', 'name_' . Yii::$app->language])->where(['cate' => 2])->andWhere(['fak_id' => $myfakultet])->orderBy(['id' => SORT_ASC])->all(), 'id', 'name_' . Yii::$app->language);
        } else $kafedralar = [];
        // $model = Center::find()->where(['cate' => 2])->orderBy(['fak_id' => SORT_ASC])->asArray()->all();//->groupBy(['fak_id'])->all();
        $fakultetlar = ArrayHelper::map(Center::find()->select(['id', 'name_' . Yii::$app->language])->where(['cate' => 1])->orderBy(['id' => SORT_ASC])->all(), 'id', 'name_' . Yii::$app->language);
        //->groupBy(['fak_id'])->all();
        //   $name = "name_" . Yii::$app->language;

        if (isset($_POST['kafedra'])) {
            $mykafedra = $_POST['kafedra'];
            $model = Hodim::find()->where(['lav_id' => [5, 6, 7, 8, 9]])->andWhere(['cate' => $mykafedra])->orderBy('name_en asc');
            $pages = new Pagination(['totalCount' => $model->count(), 'pageSize' => 200000]);
        } else {
            $model = Hodim::find()->where(['lav_id' => [5, 6, 7, 8, 9]])->orderBy('name_en asc');
            $pages = new Pagination(['totalCount' => $model->count(), 'pageSize' => 200000]);
        };

        $pages->pageSizeParam = false;
        $model = $model->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('publactivity', ['model' => $model, 'pages' => $pages, 'fakultetlar' => $fakultetlar, 'kafedralar' => $kafedralar]);

    }


    public function actionSearch($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new \yii\db\Query;
            $query->select('id, name_' . Yii::$app->language . ' AS text')
                ->from('hodim')
                ->where(['like', 'name_' . Yii::$app->language, $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Hodim::find($id)->name_uz];
        }
        return $out;
    }

    public function actionView(int $id = null)
    {
        $model = Hodim::findOne(['id' => $id]);
//        var_dump($model);
//        exit();
        if ($model !== null) {

            return $this->render('view',
                ['model' => $model]);
        }
        return $this->redirect(['staff/index']);
    }

}
