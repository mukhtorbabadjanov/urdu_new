<?php


namespace app\controllers;


use yii\web\Controller;

class InterrelationshipController extends Controller
{
	public $layout = 'interrelationship';

	public function actionProjects(){
		return $this->render('projects');
	}


}