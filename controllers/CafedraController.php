<?php


namespace app\controllers;


use yii\web\Controller;

class CafedraController extends Controller
{
    public $layout = 'l-cafedra';

    public function actionCafedra(){
        return $this->render('cafedra');
    }

}