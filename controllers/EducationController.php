<?php


namespace app\controllers;


use yii\web\Controller;

class EducationController extends Controller
{
	public $layout = 'education';
	public function actionInnovationInEducation(){

		return $this->render('innovation-in-education');
	}

	public function actionBachelorsDegree(){
		return $this->render('bachelors-degree');
	}

	public function actionMastersDegree(){
		return $this->render('masters-degree');
	}

	public function actionPhdDegree(){
		return $this->render('phd-degree');
	}

	public function actionEnglishTaught(){
		return $this->render('english-taught');
	}

	public function actionFoundationProgram(){
		return $this->render('foundation-program');
	}

	public function actionDistanceEducation(){
		return $this->render('distance-education');
	}

}