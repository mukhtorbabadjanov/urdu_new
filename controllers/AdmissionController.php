<?php


namespace app\controllers;


use yii\web\Controller;

class AdmissionController extends Controller
{
	public $layout = 'admission';

	public function actionAdmissions()
	{
		return $this->render('admissions');
	}

	public function actionWhyUrsu()
	{
		return $this->render('why-ursu');
	}

	public function actionAdmissionTrajectory()
	{
		return $this->render('admission-trajectory');
	}

	public function actionGovernmentScholarship(){
		return $this->render('government-scholarship');
	}

	public function actionWhenYouArrive(){
		return $this->render('when-you-arrive');
	}

	public function actionStudentVisa(){
		return $this->render('student-visa');
	}

	public function actionImportantInformation(){
		return $this->render('important-information');
	}
}