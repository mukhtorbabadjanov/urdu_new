<?php


namespace app\controllers;


use app\models\Photogallery;
use yii\web\Controller;

class NewsController extends Controller
{
	public $layout = 'l-news';

	public function actionNews(){
		return $this->render('news');
	}

	public function actionEvents(){
		return $this->render('events');
	}

	public function actionPhotogallery(){
	    $gallery = Photogallery::find()->all();
		return $this->render('photogallery',[
		    'gallery' => $gallery
        ]);
	}

	public function actionVideogallery(){
		return $this->render('videogallery');
	}
}