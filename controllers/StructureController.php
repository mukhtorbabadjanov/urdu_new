<?php


namespace app\controllers;


use yii\web\Controller;

class StructureController extends Controller
{
	public $layout = 'l-structure';

	public function actionFaculties(){
		return $this->render('faculties');
	}
	public function actionCentersDepartments(){
	    return $this->render('centers-departments');
    }


}
