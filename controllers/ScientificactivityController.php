<?php


namespace app\controllers;


use yii\web\Controller;

class ScientificactivityController extends Controller
{
	public $layout = 'scientificactivity';

	public function actionPFundamental()
	{
		return $this->render('p-fundamental');
	}

	public function actionCInternational(){
		return $this->render('c-international');
	}

	public function actionCLocal(){
		return $this->render('c-local');
	}

	public function actionCSummercamp(){
		return $this->render('c-summercamp');
	}

	public function actionEPartnership(){
		return $this->render('e-partnership');
	}

	public function actionETechnopark(){
		return $this->render('e-technopark');
	}

	public function actionOnFundamental(){
		return $this->render('on-fundamental');
	}

	public function actionOnInnovation(){
		return $this->render('on-innovation');
	}

	public function actionOnStartup(){
		return $this->render('on-startup');
	}

	public function actionPInnovation(){
		return $this->render('p-innovation');
	}

	public function actionPStartup(){
		return $this->render('p-startup');
	}

}