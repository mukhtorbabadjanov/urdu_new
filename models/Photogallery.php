<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * This is the model class for table "photogallery".
 *
 * @property int $id
 * @property string $image
 * @property string|null $title_ru
 * @property string|null $title_en
 * @property string|null $title_uz
 * @property string|null $text_uz
 * @property string|null $text_en
 * @property string|null $text_ru
 * @property int|null $type
 * @property string $image_cat
 */
class Photogallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'photogallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text_uz', 'text_en', 'text_ru','image_cat','image'], 'string'],
            [['type'], 'integer'],
            [['image', 'title_ru', 'title_en', 'title_uz', 'image_cat'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'title_ru' => 'Title Ru',
            'title_en' => 'Title En',
            'title_uz' => 'Title Uz',
            'text_uz' => 'Text Uz',
            'text_en' => 'Text En',
            'text_ru' => 'Text Ru',
            'type' => 'Type',
            'image_cat' => 'Image Cat',
        ];
    }
    public function beforeSave($insert)
    {
        $file = UploadedFile::getInstance($this, 'image');
        VarDumper::dump($file);
        $path = Yii::$app->basePath . '/uploads/images/gallery/';
        $name = 'photo_' . date('Y-m-d') . Yii::$app->security->generateRandomString(20) . '.' . $file->extension;

        if ($file != null && $file->saveAs($path . '/' . $name)) {
            $this->image = $name;

        }
        $thumb_name = 'thumb_' . date('Y-m-d') . Yii::$app->security->generateRandomString(20) . '.' . $file->extension;
        Image::thumbnail($path.'/'.$name,400,300)->save($path.'/thumb/'.$thumb_name);
        $this->image_cat = $thumb_name;
        return true && parent::beforeSave($insert);

    }

    public function afterDelete()
    {
        $path = Yii::getAlias('@public/images/gallery');
        if (file_exists(($path . $this->img)))
            unlink($path . $this->img);
        return true;
    }
}
