<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Catigory]].
 *
 * @see Catigory
 */
class CatigoryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Catigory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Catigory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
