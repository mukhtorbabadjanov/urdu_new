<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%lavozimlar}}".
 *
 * @property int $id
 * @property string|null $name_uz
 * @property string|null $name_ru
 * @property string|null $name_en
 * @property string $cat
 */
class Lavozimlar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lavozimlar}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat'], 'required'],
            [['cat'], 'string'],
            [['name_uz', 'name_ru', 'name_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_uz' => Yii::t('app', 'Name Uz'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_en' => Yii::t('app', 'Name En'),
            'cat' => Yii::t('app', 'Cat'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return LavozimlarQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LavozimlarQuery(get_called_class());
    }



}
