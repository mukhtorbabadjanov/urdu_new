<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%post_lang}}".
 *
 * @property int $id
 * @property int|null $posts_id
 * @property string $language
 * @property string $title
 * @property string $short_text
 * @property string $full_text
 *
 * @property Post $posts
 */
class PostLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%post_lang}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['posts_id'], 'integer'],
            [['language', 'title', 'short_text', 'full_text'], 'required'],
            [['short_text', 'full_text'], 'string'],
            [['language'], 'string', 'max' => 3],
            [['title'], 'string', 'max' => 255],
            [['posts_id', 'language'], 'unique', 'targetAttribute' => ['posts_id', 'language']],
            [['posts_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['posts_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'posts_id' => Yii::t('app', 'Posts ID'),
            'language' => Yii::t('app', 'Language'),
            'title' => Yii::t('app', 'Title'),
            'short_text' => Yii::t('app', 'ShortText'),
            'full_text' => Yii::t('app', 'FullText'),
        ];
    }

    /**
     * Gets query for [[Posts]].
     *
     * @return \yii\db\ActiveQuery|PostQuery
     */
    public function getPosts()
    {
        return $this->hasOne(Post::className(), ['id' => 'posts_id']);
    }

    /**
     * {@inheritdoc}
     * @return PostLangQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PostLangQuery(get_called_class());
    }
}
