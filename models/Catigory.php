<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%catigory}}".
 *
 * @property int $id
 * @property string $name_uz
 * @property string $name_ru
 * @property string $name_en
 * @property int|null $parent
 * @property string $tag
 * @property string $url
 *
 * @property Post[] $posts
 */
class Catigory extends \yii\db\ActiveRecord
{
     public $template;
	 /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%catigory}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_uz', 'name_ru', 'name_en', 'tag', 'url'], 'required'],
            [['parent'], 'integer'],
            [['name_uz', 'name_ru', 'name_en', 'url'], 'string', 'max' => 256],
            [['tag'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_uz' => Yii::t('app', 'Name Uz'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_en' => Yii::t('app', 'Name En'),
            'parent' => Yii::t('app', 'Parent'),
			 'template' => Yii::t('app', 'Template'),
            'tag' => Yii::t('app', 'Tag'),
            'url' => Yii::t('app', 'Url'),
        ];
    }

    /**
     * Gets query for [[Posts]].
     *
     * @return \yii\db\ActiveQuery|PostQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['cat_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return CatigoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CatigoryQuery(get_called_class());
    }
    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }
}
