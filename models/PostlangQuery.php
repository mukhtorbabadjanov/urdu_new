<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PostLang]].
 *
 * @see PostLang
 */
class PostLangQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PostLang[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PostLang|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
