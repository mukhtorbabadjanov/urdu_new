<?php

namespace app\models;

use creocoder\translateable\TranslateableBehavior;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;
use yii\db\ActiveQuery;
use Yii;

/**
 * This is the model class for table "{{%post}}".
 *
 * @property int $id
 * @property int $cat_id
 * @property string|null $creat_at
 * @property int|null $user_id
 * @property string|null $image
 * @property string|null $updated_at
 * @property int $allow
 *
 * @property User $user
 * @property Catigory $cat
 * @property PostLang[] $postLangs
 */
class Post extends \yii\db\ActiveRecord
{


    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::class,
                'translationAttributes' => ['title', 'short_text', 'full_text'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }


    public function getTranslations()
    {
        return $this->hasMany(PostLang::class, ['posts_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%post}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_id', 'allow'], 'required'],
            [['cat_id', 'user_id', 'allow'], 'integer'],
            [['creat_at', 'updated_at'], 'safe'],
            [['image'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Catigory::className(), 'targetAttribute' => ['cat_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cat_id' => Yii::t('app', 'Catigory'),
            'creat_at' => Yii::t('app', 'CreatAt'),
            'user_id' => Yii::t('app', 'User ID'),
            'image' => Yii::t('app', 'Image'),
         //   'imgfile' => Yii::t('app', 'Imgfile'),
            [['imgfile'], 'image', 'extensions' => 'jpg, jpeg', 'minWidth' => 250, 'minHeight' => 250, 'maxSize' => 100 * 1024],
            'updated_at' => Yii::t('app', 'UpdatedAt'),
            'allow' => Yii::t('app', 'Allow'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[Cat]].
     *
     * @return \yii\db\ActiveQuery|CatigoryQuery
     */
    public function getCat()
    {
        return $this->hasOne(Catigory::className(), ['id' => 'cat_id']);
    }

    /**
     * Gets query for [[PostLangs]].
     *
     * @return \yii\db\ActiveQuery|PostLangQuery
     */
    public function getPostLangs()
    {
        return $this->hasMany(PostLang::className(), ['posts_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return PostQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PostQuery(get_called_class());
    }




    public function getItems()
    {
        return $this->hasMany(PostLang::className(), ['posts_id' => 'id']);
}
}
