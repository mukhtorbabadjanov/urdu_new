<?php
return [
'Urgench State University'=>'Urganch Davlat Universiteti',
'news'=>'Yangiliklar',
    'See all'=>'Barchasi',
    "Events"=>'E\'lonlar',
    'More'=>'Batafsil',
'Fakultet'=>'Fakultet',
'Kurs'=>'Kurs',
'Guruh'=>'Guruh',
'Home'=>'Bosh sahifa',
'Login'=>'Kirish',
'Logout'=>'Chiqish',
'Select'=>'--Tanlang--',
'Begin'=>'Boshlash',
'Names of disciplines'=>'Fanning nomi',
'Teachers'=>"O'qituvchilar",
    'Update'=>'O\'zgartirish',
    'Delete'=>'O\'chirish',
    'Create'=>'Yaratish',
    'Save'=>'Saqlash',

'Send'=>"Jo'natish",
'Cancel'=>"Bekor qilish",
    'Catigory'=>'Yangilik kategoriyasi',
    'Categories'=>'Kategoriyalar',
    'creat_at'=>'Yaratilgan vaqti',
    'User'=>'Foydalanuvchi',
    'Users'=>'Foydalanuvchilar',
    'Imgfile'=>'Rasm file',
    'Title'=>"Sarlavha",
    'ShortText'=>'Qisqacha mazmuni',
    'FullText'=>'Matni',
    'Allow'=>'Bosh sahifaga chop qilish',
    'CreatAt'=>'Yaratilgan vaqti',
    'UpdatedAt'=>'O\'zgartirilgan vaqti',
    'Image'=>'Rasm',
    'User ID'=>"Foydalanuvchi ID raqami",
    'Status'=>"Statusi",
    "Username"=>"Foydalanuvchi nomi",
    "Email"=>"E-pochta",

    'Attention'=>'Diqqat',
    'Are you sure you want to delete this item?'=>'Haqiqatan ham bu yozuvni o‘chirmoqchimisiz?',
    'Yes'=>'Ha',
    'No'=>"Yo'q",
    'The record was successfully deleted'=>"Yozuv muvaffaqiyatli o'chirildi",
    'The record was successfully updated'=>"Yozuv muvaffaqiyatli o'zgartirildi",


];