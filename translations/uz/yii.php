<?php

return [
    '{attribute} cannot be blank.' => '"{attribute}" maydoni bo\'sh bo\'lmasligi lozim.',
    'The verification code is incorrect.' => 'Неправильный код подтверждаения.',
    'Home' => 'Bosh sahifa',
    'You are not allowed to perform this action.' => 'Sizga buni amalga oshirish huquqi berilmagan.',
    '{attribute} is not a valid email address.' => '"{attribute}" manzil noto\'g\'ri',
    
      
];